//var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var Timer = require('./src/timer');


module.exports.run = function (worker) {
    var debugFlag = true;
    if(debugFlag) console.log(' >> Worker PID:', process.pid);


    function startServer () {
        var matchingQueue = [];
        var rooms = {};
        var usersData = {};
        var scServer = worker.scServer;
        var url = 'mongodb://twa:degra1991@game.pydge.com:27017/twa';

        MongoClient.connect(url, function(err, db) {
            if (err) throw  err;
            socketInitiate(db);
            setInterval(matchPlayers,500);
            if (debugFlag) console.log(new Date().toUTCString(),"|","Connected correctly to MongoDB.");
        });

        function socketInitiate(db){
            scServer.on('connection', connection);

            function connection (socket) {
                socket.on("disconnect", disconnect);
                socket.on("login", login);
                socket.on("updateUserData", updateUserData);
                socket.on("createSlot", createSlot);
                socket.on("deleteSlot", deleteSlot);
                socket.on("selectSlot", selectSlot);
                socket.on("updateSlot", updateSlot);
                socket.on("joinQueue", joinQueue);
                socket.on("leaveQueue", leaveQueue);
                socket.on("joinRoom", joinRoom);
                socket.on("leaveRoom", leaveRoom);
                socket.on("confirmRoom", confirmRoom);
                socket.on("readyToPlay", readyToPlay);
                socket.on("broadCastPlayerMove", broadCastPlayerMove);
                socket.on("broadCastPlayerStatus", broadCastPlayerStatus);
                socket.on("broadCastPlayerSkill", broadCastPlayerSkill);
                socket.on("updateScore", updateScore);
                socket.on("getGameResult", getGameResult);

                function disconnect () {
                    if (debugFlag) console.log(new Date().toUTCString(),"|","player disconnect ",socket.username);
                    var channels = socket.subscriptions();
                    channels.forEach(function(channel){
                        if (channel === "matchingQueue" ) leaveQueue();
                        if (channel.match(/room-/gi)) leaveRoom(channel);
                    });
                }
                function login (params) {
                    var username = params.username;
                    var password = params.password;
                    db.collection("users").findOne({username:username,psw:password},{"_id":0,"psw":0},function(err,doc){
                        if (err) { socket.emit("showMessage", { page:"login", message:"Server currently not available try later!" }); return; }
                        if (!doc) { socket.emit("showMessage",{ page:"login", message: "Username or Password are invalid!"}); return; }
                        usersData[username] = doc;
                        socket.username = doc.username;
                        socket.token = doc.token;
                        socket.emit("login",{ token:socket.token, username:socket.username, userData: usersData[username] });
                        if (debugFlag) console.log(new Date().toUTCString(),"|","player connected ",socket.username);
                    });
                }

                /** userData **/
                function updateUserData (username) {
                    db.collection("users").findOne({username:username},{"_id":0,"username":1,"status":1,"type":1,"slots":1},function(err,doc){
                        usersData[doc.username] = doc;
                        socket.emit("updateUserData",usersData[username]);
                        if (debugFlag) console.log(new Date().toUTCString(),"|","player userData requested ",socket.username);
                    });
                }

                /** slots  **/
                function createSlot (params) {  //data:slot ,index:index


                    var userData = usersData[socket.username];
                    var slot  = {
                        experience : 0,
                        rank : 0 ,
                        gems : 0,
                        adena: 0,
                        avatar:{
                            aspect : {},
                            equipment : {},
                            skills : {
                                a : {},
                                b : {},
                                c : {}
                            },
                            attributes: {
                                stamina: 0,
                                armor: 0,
                                str: 0,
                                dex: 0,
                                int: 0,
                                lucky: 0
                            },
                            mastery: {
                                "shield":0,
                                "sword":0,
                                "axe":0,
                                "spear":0,
                                "staff":0,
                                "bow":0
                            }
                        },
                        inventory : {
                            skills : [
                                {
                                    name:"slam",
                                    id:"slam",
                                    icon:"slamIcon",
                                    description:"a basic melee attack",
                                    bind:"A"
                                },
                                {
                                    name:"hook",
                                    id:"hook",
                                    icon:"hookIcon",
                                    description:"hook the enemy and bring him to you",
                                    bind:"B"
                                },
                                {
                                    name:"Bear Trap",
                                    id:"bearTrap",
                                    icon:"bearTrapIcon",
                                    description:"bearTrap",
                                    bind:"C"
                                }
                            ],
                            equip : [
                                {
                                    name : "common armor",
                                    type : "armor",
                                    status : 101,
                                    bind : true,
                                    attributes : { str:1, dex:1, int:1 },
                                    icon : "common-armor",
                                    render : "common"
                                },
                                {
                                    name : "common sword",
                                    type : "weapon",
                                    class: "sword",
                                    status : 101,
                                    bind : true,
                                    attributes : { str:1, dex:1, int:1 },
                                    icon : "common-sword",
                                    render : "common-sword"
                                },
                                {
                                    name : "common gloves",
                                    type : "gloves",
                                    status : 101,
                                    bind : true,
                                    attributes : { str:1, dex:1, int:1 },
                                    icon : "common-gloves",
                                    render : "common"
                                },
                                {
                                    name : "common boots",
                                    type : "boots",
                                    status : 101,
                                    bind : true,
                                    attributes : { str:1, dex:1, int:1 },
                                    icon : "common-boots",
                                    render : "common"
                                },
                                {
                                    name : "common shield",
                                    type : "shield",
                                    status : 101,
                                    bind : true,
                                    attributes : { str:1, dex:1, int:1 },
                                    icon : "common-shield",
                                    render : "common-shield"
                                }
                            ],
                            mats : []
                        }

                    };
                    var equipment = slot.avatar.equipment;
                    var skills = slot.avatar.skills;
                    slot.avatar.aspect = params.data.aspect;
                    /** equip **/
                    equipment.weapon = slot.inventory.equip[1];
                    equipment.helmet = {};
                    equipment.armor = slot.inventory.equip[0];
                    equipment.gloves = slot.inventory.equip[2];
                    equipment.boots = slot.inventory.equip[3];
                    equipment.shield = slot.inventory.equip[4];
                    equipment.offHand = {};
                    /** skills **/
                    skills.a = slot.inventory.skills[0];
                    skills.b = slot.inventory.skills[1];
                    skills.c = slot.inventory.skills[2];
                    userData.slots[params.index] = slot;
                    var updateObject = {"$set": { } };
                    updateObject["$set"]["slots."+params.index] = slot;
                    db.collection("users").updateOne({username:socket.username},updateObject,function(err,res){
                        if(err) return console.log("error Creation!!");
                        socket.emit("updateUserData",usersData[socket.username]);
                        if (debugFlag) console.log(new Date().toUTCString(),"|","new slot created ",socket.username);
                        socket.emit("slotCreated");
                    })
                }
                function deleteSlot (params) {  //data:slot ,index:index
                    var updateObject = {"$unset": { } };
                    updateObject["$unset"]["slots."+params.index] = 1;

                    db.collection("users").updateOne({username:socket.username},updateObject,function(err,res) {
                        db.collection("users").updateOne({username: socket.username}, {"$pull": {slots: null}}, function (err2, res2) {
                            if(err || err2) return console.log("error delete!!");
                            updateUserData(socket.username);
                            socket.emit("slotDeleted");
                        })
                    });


                    db.collection("users").updateOne({username:socket.username},updateObject,function(err,res){
                        if(err) return console.log("error Creation!!");
                        socket.emit("updateUserData",usersData[socket.username]);
                        if (debugFlag) console.log(new Date().toUTCString(),"|","new slot created ",socket.username);
                        socket.emit("slotCreated");
                    })
                }
                function selectSlot (slotIndex) {
                    usersData[socket.username].slotIndex = slotIndex;
                }
                function updateSlot (params) {}

                /** queue **/
                function joinQueue () {
                    if (matchingQueue.indexOf(socket.username) >= 0) return;
                    matchingQueue.push(socket.username);
                    socket.emit("joinedQueue");
                    if (debugFlag) console.log(new Date().toUTCString(),"|","player join queue ",socket.username);
                }
                function leaveQueue () {
                    var index = matchingQueue.indexOf(socket.username);
                    if (index >= 0)  matchingQueue.splice(index,1);
                    socket.emit("leftQueue");
                    if (debugFlag) console.log(new Date().toUTCString(),"|","player left queue ",socket.username);
                }

                /** room **/
                function joinRoom (roomId) {
                    scServer.exchange.publish(roomId, {event: "playerJoined", data: usersData[socket.username]});
                    rooms[roomId].players[socket.username] = usersData[socket.username];
                    scServer.exchange.publish(roomId, {event: "updateRoom", data: rooms[roomId]});
                }
                function leaveRoom (roomId) {
                    var room = rooms[roomId];
                    room.status = "close";
                    scServer.exchange.publish(roomId,{event:"leaveRoom",data:room});
                }

                function confirmRoom (roomId) {
                    var room = rooms[roomId];
                    room.players[socket.username].confirm = true;
                    if(!room.userHost) room.userHost = socket.username;
                    var players = Object.keys(room.players);
                    var confirmCount = 0;
                    players.forEach(function (playerName){
                        if(room.players[playerName].confirm) confirmCount++;
                        room.players[playerName].team = (confirmCount % 2)? "red" : "blue";
                    });
                    if(confirmCount !== players.length)  return;

                    scServer.exchange.publish(roomId,{event:"createGame",data:room});
                }
                function readyToPlay(roomId){
                    var room = rooms[roomId];
                    room.players[socket.username].ready = true;
                    if(!room.userHost) room.userHost = socket.username;
                    var players = Object.keys(room.players);
                    var readyCount = 0;
                    players.forEach(function (playerName){
                        if(room.players[playerName].ready) readyCount++;
                    });
                    if(readyCount !== players.length)  return;
                    scServer.exchange.publish(roomId,{event:"startGame",data:room});
                    room.timer.onTick(function(timer) { scServer.exchange.publish(roomId,{event:"updateGameTime",data:room}) });
                    room.timer.onEnd(function(timer) {  scServer.exchange.publish(roomId,{event:"closeGame",data:room}) });
                    room.timer.play(120);
                }

                function updateScore(params) {
                    if (debugFlag) console.log(new Date().toUTCString(),"|",socket.username,"|",params);
                    rooms[params.room].score = params.data;
                    scServer.exchange.publish(params.room,params.data);
                }

                function broadCastPlayerMove (params){
                    if (debugFlag) console.log(new Date().toUTCString(),"|",socket.username,"|",params);
                    scServer.exchange.publish(params.room,params);
                }
                function broadCastPlayerStatus (params){
                    if (debugFlag) console.log(new Date().toUTCString(),"|",socket.username,"|",params);
                    scServer.exchange.publish(params.room,params);
                }
                function broadCastPlayerSkill (params){
                    if (debugFlag) console.log(new Date().toUTCString(),"|",socket.username,"|",params);
                    scServer.exchange.publish(params.room,params);
                }

                function getGameResult (roomId) {
                    var params = {event:"updateFinalScore"};
                    var room = rooms[roomId];
                    var clientTeam = room.players[socket.username].team;
                    var teamsSorted = Object.keys(room.score).sort(function(a,b){return room.score[b]-room.score[a]});
                    var matchGains = { // [ xp , adena ]
                        win: [200, 200],
                        draw: [50, 50],
                        lose: [50, 20]
                    };
                    room.players[socket.username].ready = false;
                    room.players[socket.username].confirm = false;
                    room.status = "close";
                    params.room = rooms[roomId];
                    if (teamsSorted[0] === clientTeam && room.score[clientTeam] > room.score[teamsSorted[1]]) params.result = "win";
                    else if (room.score[clientTeam]  ===  room.score[teamsSorted[0]]) params.result = "draw";
                    else params.result = "lose";
                    // updateUserXpAdena(socket.username, matchGains[params.result], function () {
                    //     if (debugFlag) console.log(new Date().toUTCString(),roomId,socket.username,"|",params.result);
                    //     scServer.exchange.publish(socket.username,params);

                    //     db.collection('users').findOne(
                    //         {username:socket.username},
                    //         {
                    //             _id: false,
                    //             username: true,
                    //             'slots': true
                    //         },
                    //         function (err, doc) {
                    //         if (err) {
                    //             console.log(err);
                    //             return;
                    //         }
                    //         console.log(doc);
                    //     });
                    // });
                    if (debugFlag) console.log(new Date().toUTCString(),roomId,socket.username,"|",params.result);
                    scServer.exchange.publish(socket.username,params);
                }

                function updateUserXpAdena(user, matchGains, callback) { // work in progress, needs refactor
                    /* Todo */
                    /* find using token
                       get slot from client */
                    
                    var xp = matchGains[0];
                    var adena = matchGains[1];
                    console.log('xp - ' + xp);
                    console.log('adena - ' + adena);

                    // for debuging
                    db.collection('users').findOne(
                        {username:user},
                        {
                            _id: false,
                            username: true,
                            'slots': true
                        },
                        function (err, doc) {
                        if (err) {
                            console.log(err);
                            return;
                        }
                        console.log(doc);

                        db.collection('users').updateOne(
                            {'username':user},
                            {'$inc': {
                                'slots.0.experience': xp,
                                'slots.0.adena': adena
                                }
                            },
                            function (err, res) {
                                if (err) console.log(err);

                                callback();
                            }
                        );
                    });
                }
            }
        }
        function matchPlayers (){
            if (matchingQueue.length < 2) return;
            var room = new Room("room-"+new Date()*1000+parseInt(Math.random()*10));
            var playerId1 = matchingQueue[Math.floor(Math.random()*matchingQueue.length)];
            var matchingQueueTemp = matchingQueue.filter(function(obj) { return [playerId1].indexOf(obj) === -1; });
            var playerId2 = matchingQueueTemp[Math.floor(Math.random()*matchingQueueTemp.length)];
            if (!playerId1 || !playerId2) return null;
            room.players[playerId1] = usersData[playerId1];
            room.players[playerId2] = usersData[playerId2];
            rooms[room.id] = room;
            scServer.exchange.publish(playerId1,{event:"setRoom",data:room});
            scServer.exchange.publish(playerId2,{event:"setRoom",data:room});
            matchingQueue.splice(matchingQueue.indexOf(playerId1),1);
            matchingQueue.splice(matchingQueue.indexOf(playerId2),1);
            if (debugFlag) console.log(new Date().toUTCString(),"|","players matched ",room.id, playerId1);
            if (debugFlag) console.log(new Date().toUTCString(),"|","players matched ",room.id, playerId2);
        }
        function Room(roomId) {
            this.id = roomId.toString();
            this.players = {};
            this.status = "open";            // 1 open, 2 inGame, 3 end
            this.timer = new Timer();
            this.time = this.timer.seconds;
            this.score = {
                red: 0,
                blue:0
            };
        }
    }
    startServer();
    /*
    var timer = new Timer();
    timer.play();
    timer.onTick(console.log);
    */
};

