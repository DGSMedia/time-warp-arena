var express = require("express");
var MongoClient = require('mongodb').MongoClient;
var bodyParser = require('body-parser');
var app = express();
var mongoUrl = "mongodb://localhost:27017/exampleDb";
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
app.use(allowCrossDomain);
app.post("/register",register);
app.post("/login",login);
app.listen(3001,startServer);

function startServer(){
    console.log("Login server live at Port 3001");
}
function allowCrossDomain(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
}
function validateData(data,fields){
    var conditions = [];
    if(fields.indexOf("email") >= 0){
        conditions.push(!data || !data.email || data.email.indexOf("@") < 0 || data.email.indexOf(".") < 0);
    }
    if(fields.indexOf("username") >= 0){
        conditions.push(!data || !data.username);
    }
    if(fields.indexOf("psw") >= 0){
        conditions.push(!data || !data.psw);
    }
    // if(conditions.indexOf(true) >= 0) return false;
    // return true;
    return !(conditions.indexOf(true) >= 0);
}

function login(req,res){
    var data = {
        email : req.body.email,
        psw : req.body.psw,
        username : req.body.username
    };
    var isValidData = validateData(data,["username","psw"]);
    if(!isValidData) return res.send({"status":-1,"message":"invalid Data"});
    MongoClient.connect(mongoUrl, function(err,db) {
        db.collection("test").findOne({"username":data.username,"psw":data.psw},{},function(err,result){
            if(!result) res.send({"status":-3,"message":"username or password not valid!"});
            else res.send({"status":1,"message":"login success!"});
        })
    })
}

function register(req,res){
    var data = {
        email : req.body.email,
        psw : req.body.psw,
        username : req.body.username
    };
    var isValidData = validateData(data,["email","username","psw"]);
    if(!isValidData) return res.send({"status":-1,"message":"invalid Data"});
    MongoClient.connect(mongoUrl, function(err,db) {
        db.collection("test").findOne({$or:[{"email":data.email},{"username":data.username}]},{},function(err,result){
            if (err) console.log(err);
            else if(result){
                return res.send({"status":-2,"message":"user already present"});
            } else {
                db.collection("test").insert({"email":data.email,"username":data.username,"psw":data.psw},{},function(err,insRes){
                    if (err) return res.send({"status":-3,"message":"server busy! try again"});
                    return res.send({"status":1,"message":"registration complete"});
                });
            }
        })
    });
}