
var mobileDetect = new MobileDetect(window.navigator.userAgent);
window.onload = function () {
    'use strict';
    var game;
    var ns = window['phaser'];
    game = new Phaser.Game(config.canvas.width, config.canvas.height, Phaser.AUTO, 'phaser-game');
    game.socket = new Client(game).create();
    game.state.add('boot', ns.Boot);
    game.state.add('version', ns.Version);
    game.state.add('selectCharacter', ns.selectCharacter);
    game.state.add('createCharacter', ns.createCharacter);
    game.state.add('preloader', ns.Preloader);
    game.state.add('login', ns.Login);
    game.state.add('home', ns.Home);
    game.state.add('inventory', ns.Inventory);
    game.state.add('room', ns.Room);
    game.state.add('loading', ns.Loading);
    game.state.add('game', ns.Game);
    game.state.add('gameResult', ns.GameResult);
    game.state.start('boot');
   // window.WebFontConfig = { google: { families: ['Ubuntu'] } };
};