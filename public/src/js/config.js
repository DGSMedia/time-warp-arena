var config = {
    server:{
        login:'http://localhost:3001/login',
        socketLocal:'http://localhost:3000',
        socket:'http://game.pydge.com:3000'
    },
    canvas:{
        width: (window.innerWidth < 1280)? 1280 : window.innerWidth,
        height: (window.innerHeight < 720)? 720 : window.innerHeight
    },
    interface:{
        fontStyles : {
            title: { font: '45px Ubuntu', fill: '#ffffff', fontWeight: 'bold', stroke:"#000000", strokeThickness:4 },
            subTitle: { font: '35px Ubuntu', fill: '#ffffff', fontWeight: 'bold', stroke:"#000000", strokeThickness:3 },
            creationLabel: {font : "40px Ubuntu", fill:"#fce000", fontWeight: 'bold', stroke:"#000000", strokeThickness:4 },
            inventoryLabel : {font: '30px Ubuntu', fill: '#ffffff', fontWeight: 'bold',stroke:"#000000", strokeThickness:4},
            textCommon: { font: '25px Ubuntu', fill: '#ffffff', fontWeight: 'bold', stroke:"#000000", strokeThickness:3 },
            textDamage: { font: '35px Ubuntu', fill: '#ffffff', fontWeight: 'bold', stroke:"#000000", strokeThickness:3 },
            textDamageCrit: { font: '35px Ubuntu', fill: '#fce000', fontWeight: 'bold', stroke:"#000000", strokeThickness:3 },
            textBuff: { font: '35px Ubuntu', fill: '#00ff00', fontWeight: 'bold', stroke:"#000000", strokeThickness:3 },
            gameUsername : { font: "20px Ubuntu", fill: "#ffffff",  fontWeight:"bold", stroke:"#000000", strokeThickness:2, align: "center" },
            defaultButton : {font : "32px Ubuntu", fill:"#ffffff", fontWeight: 'bold', stroke:"#000000", strokeThickness:4, textTransform:"uppercase" },
            usernameField: {font: '21px Ubuntu', width:250, fill: '#fff', fontWeight: 'bold',fillAlpha : 0, placeHolder: 'USERNAME'},
            pswField: { font: '21px Ubuntu' , width:250, fill: '#fff', fontWeight: 'bold',fillAlpha : 0, placeHolder: 'PASSWORD', type: Fabrique.InputType.password},
            message: { font: "22px Ubuntu", fill: "#fff", wordWrap: true, align: "center" }
        },
        healthBarStyle: {
            width: 80, height: 8, x: 0, y: 0, bg: { color: '#1a8932' }, bar: { color: '#87d804' }, animationDuration: 60, flipped: false
        }

    },
    world: {
        gravity : 900,
        time : 60
    },
    teams:{
        red:{
            baseSpanws:[250,710],
            fill:"#FF0000"
        },
        blue:{
            baseSpanws:[1480,710],
            fill:"#0000FF"
        }
    },
    customisationsAvailable : {
        "human":{
            "sex":{
                "male":{
                    "color":[
                        {red:53,green:26,blue:0},       //dark-brown
                        {red:102,green:49,blue:0},      // brown
                        {red:230,green:184,blue:0},     // blonde
                        {red:0,green:51,blue:51},       // green
                        {red:0,green:204,blue:0},       // green
                        {red:0,green:128,blue:255},     //blue
                        {red:153,green:102,blue:255},    //purple
                        {red:204,green:0,blue:0},     //red
                        {red:241,green:94,blue:0},     //orange
                        {red:173,green:173,blue:133},   //grey
                        {red:15,green:15,blue:10},      // black
                        {red:255,green:255,blue:255}    // white
                    ],
                    "style":["hair-style-one","hair-style-two","hair-style-three","hair-style-four","none"],
                    "details":["beard-style-one","none"]
                }
            }
        }
    }

};