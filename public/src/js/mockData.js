/**
 * Created by stefano on 05/05/17.
 */
// mongodb users table
var users = [
    {
        id :"fce",
        username : Math.random().toString(36).substring(2,4),
        psw : "psw",
        dob : "03-06-1991",
        status: "online",
        type:"premium",
        slots :[
            {
                experience : 0,
                rank : 0 ,
                gems : 0,
                adena: 0,
                avatar:{
                    aspect : {},
                    equipment : {},
                    skills : {
                        a : {},
                        b : {},
                        c : {}
                    }
                },
                inventory:{
                    skills : [
                        {
                            name:"slam",
                            id:"slam",
                            icon:"slamIcon",
                            description:"a basic melee attack",
                            bind:"A"
                        },
                        {
                            name:"hook",
                            id:"hook",
                            icon:"hookIcon",
                            description:"hook the enemy and bring him to you",
                            bind:"B"
                        },
                        {
                            name:"barbed trap",
                            id:"barbedTrap",
                            icon:"barbedTrapIcon",
                            description:"barbedTrap",
                            bind:"C"
                        }
                    ],
                    equip : [
                        {
                            name : "common armor",
                            type : "armor",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-armor",
                            render : "common"
                        },
                        {
                            name : "common sword",
                            type : "weapon",
                            class: "sword",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-sword",
                            render : "common-sword"
                        },
                        {
                            name : "common gloves",
                            type : "gloves",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-gloves",
                            render : "common"
                        },
                        {
                            name : "common boots",
                            type : "boots",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-boots",
                            render : "common"
                        },
                        {
                            name : "common shield",
                            type : "shield",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-shield",
                            render : "common-shield"
                        }
                    ],
                    mats : []
                },
                attributes: {
                    stamina: 0,
                    armor: 0,
                    str: 0,
                    dex: 0,
                    int: 0,
                    lucky: 0
                },
                mastery: {
                    "shield":0,
                    "sword":0,
                    "axe":0,
                    "spear":0,
                    "staff":0,
                    "bow":0
                }
            }
        ]
    }
];

var mats = [

];

var items = [
    {
        name : "armor template",
        type : "armor",
        icon : "icon",
        render : "render",
        status : 0,
        bind : false,
        attributes : {armor:20, str:1, dex:1, int:1 }
    },
    {
        name : "gloves template",
        type : "armor",
        icon : "icon",
        render : "render",
        status : 0,
        bind : false,
        attributes : {armor:5, str:1, dex:1, int:1 }
    },
    {
        name : "weapon template",
        type : "weapon",
        class : "sword",
        icon : "icon",
        render : "render",
        status : 0,
        bind : false,
        attributes : {power:5, str:1, dex:1, int:1 }
    }
];


var skills = [
    {
        id : "slam",
        name : "slam",
        icon : "slamIcon",
        power : 30,
        cooldown : 4,
        time : 10,
        speed : 10,
        boost : { str : 20, dex : 0, int :0 },
        bind : false,
        require : [ "sword", "axe", "dagger", "2h-sword", "2h-axe" ],
        description : "a basic melee attack",
        init : function (game) {
            var skill = game.add.weapon(3, 'trap');
            skill.id = this.id;
            skill.icon = this.icon;
            skill.power = this.power;
            skill.boost = this.boost;
            skill.cooldown = this.cooldown;
            skill.bulletKillType = Phaser.Weapon.KILL_LIFESPAN;
            skill.bulletLifespan = this.time * 1000;
            skill.bulletSpeed = this.speed;
            skill.fireRate = skill.cooldown * 1000;
            skill.execute = function (player) {
                var aim = (player.aim === "left")? 180: 0;
                var power = skill.power;
                var boostValue;
                for ( var attribute in skill.boost ){
                    boostValue = player.attributes[attribute] * ( skill.boost[attribute] / 100 );
                    power = power + boostValue;
                }
                skill.trackSprite(player, 0, -10, false);
                skill.bulletAngleOffset = aim;
                skill.fireAngle = aim;
                player.avatar.setAnimationByName(1,"drop-trap",false);
                player.onAction = skill.id;
                var bullet = skill.fire();
                bullet.owner = player.username;
                bullet.lucky = player.attributes.lucky;
                bullet.power = power;
                bullet.bringToTop();
                setTimeout(player.release,500);
            };
            skill.onHit = function (target,bullet) {
                if (bullet.owner === target.username) return false;
                var damage = game.calcDamage(bullet.power,bullet.lucky,1.3);
                target.damage(damage);
                game.playerHitted (target);
                game.showDamageText(game,damage,bullet.x,bullet.y);
                game.updateScore(bullet.owner,target,game);
                bullet.kill();
            };
            return skill
        }
    },
    {
        id:"hook",
        name:"hook",
        icon:"hookIcon",
        power : 30,
        cooldown : 4,
        time : 10,
        distance : 10,
        speed : 700,
        boost : { str : 20, dex : 0, int :0 },
        require : ["axe","sword","blunt","staff","dagger","bow"],
        description:"hook the enemy and bring him to you",
        int :  function () {
            var skill = game.add.weapon(2, 'hook');
            skill.require  = this.require;
            skill.power = this.power;
            skill.cooldown = this.cooldown;
            skill.bulletKillType = Phaser.Weapon.KILL_DISTANCE;
            skill.bulletKillDistance = this.distance;
            skill.bulletSpeed = this.speed;
            skill.fireRate = skill.cooldown*1000;
            skill.execute = function execute (player){
                if (!player) return;
                var angle = (player.aim === "left")? 180: 0;
                var power = skill.power;
                var boostValue;
                for ( var attribute in skill.boost ){
                    boostValue = player.attributes[attribute] * ( skill.boost[attribute] / 100 );
                    power = power + boostValue;
                }
                player.onAction = "hook";
                player.avatar.setAnimationByName(1,"hook",false);
                skill.trackSprite(player, 0, -(player.height/1.3), false);
                skill.fireAngle = angle;
                setTimeout(function(){
                    var bullet = skill.fire();
                    bullet.owner = player.username;
                    bullet.power = power;
                    bullet.lucky = player.attributes.lucky;
                    bullet.bringToTop();
                },300);
                setTimeout(player.release,500);
            };
            skill.onHit = function onHit (target,bullet){
                var ownerId = bullet.owner;
                if (ownerId === target.username) return false;
                var playerOwner = game.players[ownerId];
                var offset = (playerOwner.aim === "left")? -75 : 25;
                skill.autofire = false;
                var damage = game.calcDamage(skill.power,bullet.lucky,1.3);
                target.damage(damage);
                game.hitted (target);
                game.showDamageText(game,damage,bullet.x,bullet.y);
                bullet.kill();
                game.add.tween(target.body).to( { x: game.players[ownerId].x+offset }, 200, Phaser.Easing.Linear.None, true);
                target.onAction = "block";
                setTimeout(target.release,200);
                game.updateScore(ownerId,target,game);
            };
            return skill;


        }
    },
    {
        id:"bearTrap",
        name:"Bear Trap",
        icon:"bearTrapIcon",
        power : 30,
        cooldown : 4,
        time : 10,
        distance : 0,
        speed : 0,
        boost : { str : 20, dex : 0, int :0 },
        require : ["axe","sword","blunt","staff","dagger","bow"],
        description:"place a trap",
        int :  function () {
            var skill = game.add.weapon(2, 'bearTrap');
            skill.require  = this.require;
            skill.power = this.power;
            skill.cooldown = this.cooldown;
            skill.bulletKillType = Phaser.Weapon.KILL_LIFESPAN;
            skill.bulletLifespan = this.time * 1000;
            skill.bulletSpeed = this.speed;
            skill.fireRate = skill.cooldown * 500;
            skill.execute =  function execute(player){
                if (!player) return;
                var angle = (player.aim === "left")? 180: 0;
                var power = skill.power;
                var boostValue;
                for ( var attribute in skill.boost ){
                    boostValue = player.attributes[attribute] * ( skill.boost[attribute] / 100 );
                    power = power + boostValue;
                }
                player.onAction = "bearTrap";
                player.avatar.setAnimationByName(1,"drop-trap",false);
                skill.bulletAngleOffset = angle;
                skill.fireAngle = angle;
                skill.trackSprite(player, 0, -10, false);
                var bullet = skill.fire();
                bullet.owner = player.username;
                bullet.power = power;
                bullet.lucky = player.attributes.lucky;
                bullet.bringToTop();
                setTimeout(player.release,500);
            };
            skill.onHit = function onHit(target,bullet){
                var ownerId = bullet.owner;
                if (ownerId === target.username) return false;
                var damage = game.calcDamage(bullet.power,bullet.lucky,1.3);
                target.damage(damage);
                game.hitted (target);
                game.showDamageText(game,damage,bullet.x,bullet.y);
                bullet.kill();
                game.updateScore(ownerId,target,game);
            };


            return skill;


        }
    }
];

var test = function () {
    var skill = {};
    skill.power = 30;
    skill.execute = function () {
        console.log(this.power);
    }
    return skill;
}

