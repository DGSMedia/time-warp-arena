var Tools = {
    getJson: function (data) {
        var jsonData;
        try {jsonData = JSON.parse(data);}
        catch (err) {
            jsonData = null;
        }
        return jsonData
    },
    sendRequest: function(url,payload,callback){
        var http = new XMLHttpRequest();
        http.open("POST", url, true);
        if(payload && payload.type && payload.type === "json") http.setRequestHeader("Content-type", "application/json");
        http.onload = function(){
            callback(http.responseText);
        };
        http.send(JSON.stringify(payload.params));
    },

    createBox : function (game,x,y,width,height,color){
        var bmd = game.add.bitmapData(width,height);
        bmd.ctx.beginPath();
        bmd.ctx.rect(0,0,width,height);
        if (color) {
            bmd.ctx.fillStyle = color;
            bmd.ctx.fill();
        }
        return game.add.sprite(x, y, bmd);
    },
    createButton : function (game, x, y, text, options) {
        if(!options) options = {};
        if(!options.sprite)  options.sprite = "button-big-blue";
        if(!options.style) options.style = config.interface.fontStyles.defaultButton;
        var sprite = game.add.sprite(x, y, options.sprite);
        var spriteText = game.add.text(sprite.width/2, 35, text, options.style);
        spriteText.anchor.set(0.5);
        sprite.addChild(spriteText);
        sprite.inputEnabled = true;
        return sprite
    },
    createText : function(x,y,style,game,textContent) {
        var text = game.add.text(x, y, textContent);
        text.anchor.setTo(0.5);
        text.font = 'Ubuntu';
        for(var role in style){
            text.style[role] = style[role];
        }
        return text;
    },
    hexToRgb : function (hex) {
        var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseFloat(parseInt(result[1], 16)/255),
            g: parseFloat(parseInt(result[2], 16)/255),
            b: parseFloat(parseInt(result[3], 16)/255)
        } : null;
    },
    getLevel : function (currentExp) {
        var level = 1;
        var percentExp;
        var maxExp = 745*(level);
        while ( currentExp > maxExp ){
            maxExp = 745 * (level);
            level++;
            currentExp -=maxExp;
            if(level === 30 && currentExp >= maxExp){
                currentExp = maxExp;
            }

        }
        percentExp = (currentExp / maxExp) * 100;
        return {
            level : level,
            levelExp : currentExp,
            maxExp : maxExp,
            percentExp : percentExp
        }
    }
};
