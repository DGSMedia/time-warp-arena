var PLAYER = function (game,username,team){
    var bmd = game.add.bitmapData(40,40); bmd.ctx.beginPath(); bmd.ctx.rect(0,0,40,40);
    var playerProfile = game.GAME.room.players[username];
    var slot = playerProfile.slots[playerProfile.slotIndex];
    var teamConfig = config.teams[team];
    var posX = teamConfig.baseSpanws[0];
    var posY = teamConfig.baseSpanws[1];
    var player = game.add.sprite(posX, posY, bmd);
    game.physics.arcade.enable(player);
    player.checkWorldBounds = true;
    player.outOfBoundsKill = true;
    player.avatar = game.add.spine(0, 0, slot.avatar.aspect.race+"-"+slot.avatar.aspect.sex);
    player.avatar.setToSetupPose();
    game.loadAvatarConfig(player.avatar,slot.avatar);
    player.avatar.setAnimationByName(0, "idle", true);
    player.team = team;
    player.username = username;
    player.attributes = slot.avatar.attributes;
    player.body.gravity.y = game.GAME.config.world.gravity;
    player.body.collideWorldBounds = true;
    player.anchor.x = 0.5;
    player.anchor.y = 1;
    player.labelName = game.add.text(0, 0, username, config.interface.fontStyles.gameUsername);
    player.labelName.anchor.x = 0.5;
    player.labelName.anchor.y = 5;
    player.labelName.addColor(teamConfig.fill,0);
    player.healthBar = new HealthBar(game, config.interface.healthBarStyle);
    player.healthBar.barSprite.anchor.y = 15;
    player.healthBar.bgSprite.anchor.y = 15;
    player.health = player.attributes.stamina * 10 + 250;
    player.maxHealth = player.attributes.stamina * 10 + 250;
    player.speed = player.attributes.dex + 300;
    player.physicAttack = player.attributes.str*2.5;
    player.magicAttack = player.attributes.int*3;
    player.lucky = player.attributes.lucky;
    player.jumpPower = player.attributes.dex +550;
    player.status = [];
    player.skills = {};
    player.skills.a = game.skills[slot.avatar.skills.a.id].init(game);
    player.skills.b = game.skills[slot.avatar.skills.b.id].init(game);
    player.skills.c = game.skills[slot.avatar.skills.c.id].init(game);
    player.score = {  k:0,  d:0,  exp:0  };
    player.addChild(player.avatar);
    player.addChild(player.labelName);
    player.addChild(player.healthBar.bgSprite);
    player.addChild(player.healthBar.barSprite);

    player.updateStatus = function (status,duration) {
        if (status === "dead") player.onDead();
        if (status === "stun") player.onStun(duration);  // add timer
        if (status === "slow") console.log('slow');  // add timer
        if (status === "poison") console.log('poison');  // add timer
        if (status === "fear") console.log('fear');  // add timer
    };
    player.updatePosition = function (event){
        var action = event.action;
        var commandData = event.data;
        if (!player.alive || !action) return;
        if (!player.onAction) player.playAnimation(action);
        if (action === "idle")   player.moveTo(null);
        if (action === "run-left")  player.moveTo("left");
        if (action === "run-right") player.moveTo("right");
        if (action === "jump") player.moveTo("jump");
        if(commandData.health) player.health = commandData.health;
        if (player.username !== game.GAME.socket.username) syncPostion();
        function syncPostion(){
            if (commandData.x > player.x +10 || commandData.x < player.x-10) game.add.tween(player).to( { x: commandData.x }, 10, Phaser.Easing.Linear.None, true);
            if (commandData.y > player.y +10 || commandData.y < player.y-10) game.add.tween(player).to( { y: commandData.y }, 10, Phaser.Easing.Linear.None, true);
            //if (!player.onAction) player.playAnimation(game.GAME.socket.lastMove);
        }

    };
    player.setAim = function (direction){
        if ( direction ) player.aim = direction;
        if ((player.aim !== "left" && player.avatar.scale.x < 0) ||
            (player.aim !== "right" && player.avatar.scale.x > 0)){
            player.avatar.scale.x = (player.avatar.scale.x * -1);
        }
    };
    player.moveTo = function (direction){

        if(!direction) player.body.velocity.x = 0;

        if (direction === "left"){
            player.setAim ("left");
            player.body.velocity.x = -player.speed;
        }
        if (direction ==="right"){
            player.setAim ("right");
            player.body.velocity.x = player.speed;
        }
        if (direction === "jump"){
            player.body.velocity.y = -(player.jumpPower);
        }
    };
    player.playAnimation = function (animation){
        var avatar = player.children[0];
        if (!player.onAction && player.body.onFloor() && (animation === "run-left" || animation === "run-right")) avatar.setAnimationByName(0, "run", true);
        if (!player.onAction && player.body.onFloor() && animation === "idle") avatar.setAnimationByName(0, "idle", true);
        if (!player.onAction && player.body.onFloor() && animation === "jump") avatar.setAnimationByName(0, "jump", false);
    };
    player.respawn = function () {
        var position = game.GAME.config.teams[player.team].baseSpanws;
        player.status = [];
        player.x = position[0];
        player.y = position[1];
        player.deadConfirmed = false;
        player.revive();
        player.dead = false;
        player.alive = true;
        player.health = player.maxHealth;
    };
    player.release = function(){
        delete player.onAction
    };

    player.onDead = function(){
        if (player.username === game.GAME.socket.username)  game.world.filters = [game.grayfilter];
        setTimeout(function (){
            player.respawn();
            game.world.filters = null;
        },5000)
    };
    player.onStun = function(duration){

    };
    player.events.onOutOfBounds.add(player.onDead, game);
    return player;
};