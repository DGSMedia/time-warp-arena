function Client(GAME) {
    this.GAME = GAME;
    this.GAME.Debug = true;
}

Client.prototype = {
    create : function(){
        //var socket = socketCluster.connect({hostname:"game.pydge.com",port:"3000"});
        var socket = socketCluster.connect({port:"3000"});
        socket.on('error', this.onError);
        socket.on ("connect",this.onConnect);
        socket.on("disconnet",this.onDisconnect);
        socket.on ("login",this.onLogin);
        socket.on ("showMessage",this.onShowMessage);
        socket.on("updateUserData",this.onUpdateUserData);
        socket.on("joinedQueue",this.onJoinedQueue);
        socket.on("leftQueue",this.onLeftQueue);
        socket.update = function (GAME) {
            socket.GAME = GAME;
            socket.GAME.socket = socket;
        };
        socket.setRoom = this.setRoom;
        socket.updateRoom = this.updateRoom;
        socket.leaveRoom = this.leaveRoom;
        socket.createGame = this.createGame;
        socket.closeGame = this.closeGame;
        socket.globalChannelHandler = this.globalChannelHandler;
        socket.privateChannelHandler = this.privateChannelHandler;
        socket.roomChannelHandler = this.roomChannelHandler;
        return socket;
    },

    onDisconnect : function (){
        console.log("connection Lost!");
    },
    onError : function (err){
        throw 'Socket error - ' + err;
    },
    onConnect: function (){
        var socket = this;
        socket.globalChannel = socket.subscribe("global");
        socket.globalChannel.watch(function(eventObj) {socket.globalChannelHandler(socket,eventObj)});
    },
    onLogin : function(params) {    /** { token:"string", username:"string", userData:"object" } **/
        var socket = this;
        socket.token = params.token;
        socket.username = params.username;
        socket.userData = params.userData;
        socket.privateChannel = socket.subscribe(params.username);
        socket.privateChannel.watch(function(eventObj) {socket.privateChannelHandler(socket,eventObj)});
        window.isOldSlots = true;
    },
    onShowMessage : function(params) {  /** { page:"string", message:"string" } **/
        var messageContainer = this.GAME.messageContainer;
        messageContainer.message.setText(params.message);
        this.GAME.game.add.tween(messageContainer).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true);
    },
    onUpdateUserData : function (userData) {
        this.userData = userData;
        window.isOldSlots = true;
    },
    onJoinedQueue : function () {
        var messageContainer = this.GAME.messageContainer;
        messageContainer.message.setText("Looking for an opponent, be ready! the game will start soon");
        this.GAME.game.add.tween(messageContainer).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true);
        messageContainer.cancelQueueButton.alpha = 1;
    },
    onLeftQueue : function () {
        var messageContainer = this.GAME.messageContainer;
        var game = this;
        messageContainer.message.setText("You aren't looking for a game anymore.");
        messageContainer.cancelQueueButton.alpha = 0;
        this.GAME.game.add.tween(messageContainer).to( { alpha: 1 }, 500, Phaser.Easing.Linear.None, true);
        setTimeout(function(){game.GAME.game.add.tween(messageContainer).to( { alpha: 0 }, 500, Phaser.Easing.Linear.None, true);},1000);
    },
    setRoom : function (room) {
        var socket = this;
        this.updateRoom(room);
        socket.roomChannel = socket.subscribe(room.id);
        socket.roomChannel.watch(function(eventObj) {socket.roomChannelHandler(socket,eventObj)});
        socket.GAME.state.start('room');
    },
    updateRoom : function (room) {
        var socket = this;
        socket.room = room;
    },
    leaveRoom : function (room) {
        var socket = this;
        room = (!room)? socket.room : room;
        socket.unsubscribe(room.id);
        delete socket.room;
        socket.GAME.state.start('home');
    },
    createGame : function (room) {
        var socket = this;
        this.GAME.isReady = false;
        socket.GAME.room = room;
        socket.GAME.state.start('loading');
    },
    closeGame : function(room){
        var socket = this;
        socket.GAME.isReady = false;
        socket.GAME.game.moveControl.active = false;
        socket.GAME.game.world.removeAll();
        socket.emit("getGameResult",room.id);
    },
    /** CHANNELS **/
    globalChannelHandler: function (socket,eventObj) {
        console.log(eventObj);
    },
    privateChannelHandler : function (socket,eventObj) {
        if (eventObj.event === "setRoom") socket.setRoom(eventObj.data);
        if (eventObj.event === "updateFinalScore") updateFinalPrice(eventObj);
        function updateFinalPrice(eventObj) {
            socket.GAME.room.score = eventObj.room.score;
            socket.GAME.room.result = eventObj.result;
            socket.GAME.state.start('gameResult');
        }
    },
    roomChannelHandler : function (socket,eventObj){
        if (eventObj.event === "updateRoom") socket.updateRoom(eventObj.data);
        if (eventObj.event === "playerJoined") console.log(eventObj.data.username, "joined the room");
        if (eventObj.event === "leaveRoom") socket.leaveRoom(eventObj.data);
        if (eventObj.event === "createGame") socket.createGame(eventObj.data);
        if (eventObj.event === "closeGame") socket.closeGame(eventObj.data);
        if (eventObj.event === "playerMove") playerMove(this.GAME);
        if (eventObj.event === "playerStatus") playerStatus(this.GAME);
        if (eventObj.event === "playerSkill") playerSkill(this.GAME);
        if (eventObj.event === "updateScore") updateScore(eventObj);
        if (eventObj.event === "updateGameTime") updateGameTime(eventObj);


        function updateGameTime(eventObj){
            socket.GAME.game.topMenu.time.text = eventObj.data.timer.time;
        }

        function updateScore(eventObj) {
            socket.GAME.room.score = eventObj.data;
        }
        function playerMove (GAME) {
            if(!GAME || !GAME.game || !GAME.game.players) return;
            var playerId = (eventObj.data && eventObj.data.player && eventObj.data.player.id)? eventObj.data.player.id : eventObj.sender;
            var player = GAME.game.players && GAME.game.players[playerId];
            if (!player || !eventObj.action || playerId === socket.username) return null;
            player.updatePosition(eventObj)
        }
        function playerStatus (GAME) {
            var playerId = (eventObj.data && eventObj.data.player && eventObj.data.player.id)? eventObj.data.player.id : eventObj.sender;
            var player = GAME.game.players && GAME.game.players[playerId];
            player.updateStatus(eventObj.data.status,eventObj.duration);
        }
        function playerSkill (GAME) {
            var playerId = (eventObj.data && eventObj.data.player && eventObj.data.player.id)? eventObj.data.player.id : eventObj.sender;
            var player = GAME.game.players && GAME.game.players[playerId];
            player.skills[eventObj.data.skillIndex].execute(player);
        }
    }

    /*,
    logUpdate: function(msg) {
        if(this.GAME.consoleDebug) console.log("%c [SERVER UPDATE] >>> "+msg, 'background: #3f51b5; color: #fff');
    },
    updatePeople : function(serverPeople) {
        this.GAME.people = serverPeople;
    },
    setRoom : function (room) {
        this.GAME.room = room;
        this.GAME.state.start('room');
    },
    setRoomHost: function (hostId) {
        if(hostId === this.GAME.profile.username) this.isHost = true;
    },
    startGame: function (roomStr) {
        this.GAME.room = JSON.parse(roomStr);
        this.GAME.state.start('loading');
    },
    updateScore: function (scoreStr) {
        this.GAME.room.score = JSON.parse(scoreStr);
    },
    updateFinalScore: function (scoreStr) {
        this.GAME.room.score = JSON.parse(scoreStr).score;
        this.GAME.room.result = JSON.parse(scoreStr).result;
        this.GAME.state.start('gameResult');
    },
    roomExpire : function (){
        delete this.GAME.room;
        this.isHost = false;
        this.GAME.state.start('home');
    },
    broadCastEvent : function (dataStr){
        var GAME = this.GAME;
        var msg = Tools.getJson(dataStr);
        var playerId = (msg.data && msg.data.player && msg.data.player.id)? msg.data.player.id : msg.sender;
        var player = GAME.game.players && GAME.game.players[playerId];
        if (!player || !msg.event) return null;
        if (msg.event === "playerMove") player.updatePosition(msg.data);
        if (msg.event && msg.event === "playerStatus")  player.updateStatus(msg.data.status,msg.duration);
        if (msg.event && msg.event === "playerSkill")   player.skills[msg.data.skillIndex].execute(player);
        if (msg.event && msg.event === "syncEvent") {
            player.x = msg.data.player.x;
            player.y = msg.data.player.y;
            player.health = msg.data.player.health;
            player.alive = msg.data.player.alive;
            GAME.game.score.red = msg.data.world.score.red;
            GAME.game.score.blue = msg.data.world.score.blue;
        }
    }
    */
};
