var SKILLS =  function (){
    var slam = {
        id : "slam",
        name : "slam",
        icon : "slamIcon",
        power : 80,
        cooldown : 0.7,
        time : 10,
        speed : 300,
        boost : { str : 20, dex : 0, int :0 },
        bind : false,
        require : [ "sword", "axe", "dagger", "2h-sword", "2h-axe" ],
        description : "a basic melee attack",
        init : function (game) {
            var skill = game.add.weapon(3, 'bullet');
            skill.id = this.id;
            skill.icon = this.icon;
            skill.power = this.power;
            skill.boost = this.boost;
            skill.cooldown = this.cooldown;
            skill.bulletKillType = Phaser.Weapon.KILL_DISTANCE;
            skill.bulletKillDistance = 90;
            skill.bulletSpeed = this.speed;
            skill.fireRate = skill.cooldown * 1000;
            skill.execute = function (player) {
                var aim = (player.aim === "left")? 180: 0;
                var power = skill.power;
                var boostValue;
                for ( var attribute in skill.boost ) {
                    boostValue = player.attributes[attribute] * ( skill.boost[attribute] / 100 );
                    power = power + boostValue;
                }
                skill.trackSprite(player, 0, -(player.height/1.3), false);
                player.avatar.setAnimationByName(1,skill.id,false);
                skill.bulletAngleOffset = aim;
                skill.fireAngle = aim;
                player.onAction = skill.id;
                setTimeout(player.release,500);
                var bullet = skill.fire();
                if (!bullet) return;
                bullet.owner = player.username;
                bullet.lucky = player.attributes.lucky;
                bullet.power = power;
                bullet.bringToTop();
            };
            skill.onHit = function (target,bullet) {
                if (bullet.owner === target.username) return false;
                var damage = game.calcDamage(bullet.power,bullet.lucky,1.3);
                target.damage(damage);
                game.playerHitted (target);
                game.showDamageText(game,damage,bullet.x,bullet.y);
                game.updateScore(bullet.owner,target,game);
                bullet.kill();
            };
            return skill
        }
    };
    var hook = {
        id : "hook",
        name : "hook",
        icon : "hookIcon",
        power : 20,
        cooldown : 3,
        time : 10,
        distance : 400,
        speed : 700,
        boost : { str : 20, dex : 0, int :0 },
        require : ["axe","sword","blunt","staff","dagger","bow"],
        description : "hook the enemy and bring him to you",
        init :  function (game) {
            var skill = game.add.weapon(2, 'hook');
            skill.id = this.id;
            skill.icon = this.icon;
            skill.power = this.power;
            skill.boost = this.boost;
            skill.cooldown = this.cooldown;
            skill.bulletKillType = Phaser.Weapon.KILL_DISTANCE;
            skill.bulletKillDistance = this.distance;
            skill.bulletSpeed = this.speed;
            skill.fireRate = skill.cooldown*1000;
            //skill.autofire = false;
            skill.execute = function execute (player){
                if (!player) return;
                var angle = (player.aim === "left")? 180: 0;
                var power = skill.power;
                var boostValue;
                for ( var attribute in skill.boost ){
                    boostValue = player.attributes[attribute] * ( skill.boost[attribute] / 100 );
                    power = power + boostValue;
                }
                player.onAction = skill.id;
                player.avatar.setAnimationByName (1, skill.id, false);
                skill.trackSprite(player, 0, - (player.height/1.3), false);
                skill.fireAngle = angle;
                setTimeout(function(){
                    var bullet = skill.fire();
                    if(!bullet) return;
                    bullet.owner = player.username;
                    bullet.power = power;
                    bullet.lucky = player.attributes.lucky;
                    bullet.bringToTop();
                },300);
                setTimeout(player.release,500);
            };
            skill.onHit = function onHit (target,bullet){
                var ownerId = bullet.owner;
                if (ownerId === target.username) return false;
                var playerOwner = game.players[ownerId];
                var offset = (playerOwner.aim === "left")? -75 : 25;
                var damage = game.calcDamage(skill.power,bullet.lucky,1.3);
                target.damage(damage);
                game.playerHitted (target);
                game.showDamageText(game,damage,bullet.x,bullet.y);
                bullet.kill();
                game.add.tween(target.body).to( { x: game.players[ownerId].x + offset }, 200, Phaser.Easing.Linear.None, true);
                target.onAction = "block";
                setTimeout(target.release,200);
                game.updateScore(ownerId,target,game);
            };
            return skill;
        }
    };
    var bearTrap = {
        id : "bearTrap",
        name : "Bear Trap",
        icon : "bearTrapIcon",
        power : 150,
        cooldown : 4,
        time : 10,
        distance : 0,
        speed : 0,
        boost : { str : 20, dex : 0, int :0 },
        require : ["axe","sword","blunt","staff","dagger","bow"],
        description : "place a trap",
        init :  function (game) {
            var skill = game.add.weapon(2, 'bearTrap');
            skill.require  = this.require;
            skill.power = this.power;
            skill.cooldown = this.cooldown;
            skill.bulletKillType = Phaser.Weapon.KILL_LIFESPAN;
            skill.bulletLifespan = this.time * 1000;
            skill.bulletSpeed = this.speed;
            skill.fireRate = skill.cooldown * 500;
            skill.execute =  function execute (player) {
                if (!player) return;
                var angle = (player.aim === "left")? 180: 0;
                var power = skill.power;
                var boostValue;
                for ( var attribute in skill.boost ){
                    boostValue = player.attributes[attribute] * ( skill.boost[attribute] / 100 );
                    power = power + boostValue;
                }
                player.onAction = skill.id;
                player.avatar.setAnimationByName(1,"drop-trap",false);
                skill.bulletAngleOffset = angle;
                skill.fireAngle = angle;
                skill.trackSprite(player, 0, -10, false);
                setTimeout(player.release,500);
                var bullet = skill.fire();
                if(!bullet) return;
                bullet.owner = player.username;
                bullet.power = power;
                bullet.lucky = player.attributes.lucky;
                bullet.bringToTop();

            };
            skill.onHit = function onHit (target,bullet) {
                var ownerId = bullet.owner;
                if (ownerId === target.username) return false;
                var damage = game.calcDamage(bullet.power,bullet.lucky,1.3);
                target.damage(damage);
                game.playerHitted (target);
                game.showDamageText(game,damage,bullet.x,bullet.y);
                bullet.kill();
                game.updateScore(ownerId,target,game);
            };
            return skill;
        }
    };
    return {
        slam : slam,
        hook : hook,
        bearTrap : bearTrap
    };
};