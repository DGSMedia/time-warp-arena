function Client(GAME) {
    this.socket = null;
    this.GAME = GAME;
    this.GAME.consoleDebug = true;
    this.GAME.people = {};
}

Client.prototype = {
    connect: function (){
        this.GAME.socket = io.connect(config.server.socketLocal);
        this.socket = this.GAME.socket;
        return this;
    },
    create : function(){
        this.consoleDebug = true;
        this.GAME.socket = this.socket;
        this.socket.GAME = this.GAME;
        this.socket.on("logUpdate", this.logUpdate);
        this.socket.on("updatePeople", this.updatePeople);
        this.socket.on("setRoom", this.setRoom);
        this.socket.on("setRoomHost",this.setRoomHost);
        this.socket.on("startGame", this.startGame);
        this.socket.on("updateScore", this.updateScore);
        this.socket.on("updateFinalScore", this.updateFinalScore);
        this.socket.on("roomExpire", this.roomExpire);
        this.socket.on("broadCastEvent", this.broadCastEvent);
        this.socket.io.on('reconnect', this.reconnect);
        return this;
    },
    logUpdate: function(msg) {
        if(this.GAME.consoleDebug) console.log("%c [SERVER UPDATE] >>> "+msg, 'background: #3f51b5; color: #fff');
    },
    updatePeople : function(serverPeople) {
        this.GAME.people = serverPeople;
    },
    setRoom : function (room) {
        this.GAME.room = room;
        this.GAME.state.start('room');
    },
    setRoomHost: function (hostId) {
        if(hostId === this.GAME.profile.username) this.isHost = true;
    },
    startGame: function (roomStr) {
        this.GAME.room = JSON.parse(roomStr);
        this.GAME.state.start('loading');
    },
    updateScore: function (scoreStr) {
        this.GAME.room.score = JSON.parse(scoreStr);
    },
    updateFinalScore: function (scoreStr) {
        this.GAME.room.score = JSON.parse(scoreStr).score;
        this.GAME.room.result = JSON.parse(scoreStr).result;
        this.GAME.state.start('gameResult');
    },
    roomExpire : function (){
        delete this.GAME.room;
        this.isHost = false;
        this.GAME.state.start('home');
    },
    broadCastEvent : function (dataStr){
        var GAME = this.GAME;
        var msg = Tools.getJson(dataStr);
        var playerId = (msg.data && msg.data.player && msg.data.player.id)? msg.data.player.id : msg.sender;
        var player = GAME.game.players && GAME.game.players[playerId];
        if (!player || !msg.event) return null;
        if (msg.event === "playerMove") player.updatePosition(msg.data);
        if (msg.event && msg.event === "playerStatus")  player.updateStatus(msg.data.status,msg.duration);
        if (msg.event && msg.event === "playerSkill")   player.skills[msg.data.skillIndex].execute(player);
        /** currently disabled **/
        if (msg.event && msg.event === "syncEvent") {
            player.x = msg.data.player.x;
            player.y = msg.data.player.y;
            player.health = msg.data.player.health;
            player.alive = msg.data.player.alive;
            GAME.game.score.red = msg.data.world.score.red;
            GAME.game.score.blue = msg.data.world.score.blue;
        }
    },
    reconnect : function (){
       // this.emit("registerClient",this.GAME.profile.username);
    }
};


var debugFlag = true;
var username = Math.random().toString(36).substring(7);
var socket = socketCluster.connect({port:"8000"});
var room = {};
var profile = {};
socket.on('error', function (err) {
    throw 'Socket error - ' + err;
});
socket.on('connect', function () {
    socket.privateChannel = socket.subscribe(username);
    socket.globalChannel = socket.subscribe("global");
    socket.emit("register",username);
    socket.emit("getUserData",username);

    socket.privateChannel.watch(privateChannel);
    socket.globalChannel.watch(globalChannel);
});

socket.on("setUserData",setUserQueue);
socket.on("joinedQueue",joinedQueue);
socket.on("leftQueue",leftQueue);
socket.on("disconnet",function(){ console.log("connection Lost!");});

function setUserQueue (userData) {
    profile = userData;
}
function joinQueue () {
    socket.queueChannel = socket.subscribe("matchingQueue");
    socket.queueChannel.watch(function (eventObj) {
        console.log(eventObj);
    });
    socket.emit("joinQueue");
}
function leaveQueue () {
    socket.unsubscribe("matchingQueue");
    socket.emit("leaveQueue");
    delete socket.queueChannel;
}

function joinedQueue () {
    console.log("joined!");
}
function leftQueue () {
    console.log("left!");
}

function privateChannel (eventObj) {
    console.log(eventObj);
    if (eventObj.event === "setRoom") room = eventObj.data;
    console.log(room);
}
function globalChannel (eventObj) {
    console.log(eventObj);
}