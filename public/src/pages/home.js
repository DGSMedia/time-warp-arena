(function () {
    'use strict';

    function Home(GAME) {
        this.GAME = GAME;
    }

    Home.prototype = {

        preload: function (GAME) {

        },

        create: function (GAME) {
            this.background = this.add.sprite(0, 0, "spaceBackground");
            this.window = this.add.sprite(0, 0, "window");
            this.title = this.window.addChild(this.game.add.text(this.window.width/2, 20, 'MAIN MENU',config.interface.fontStyles.title));
            this.title.anchor.x = 0.5;
            this.playButton = Tools.createButton(this.game,60,200,'Play game');
            this.inventoryButton = Tools.createButton(this.game,60,260,'Inventory');
            this.friendsButton = Tools.createButton(this.game,60,320,'Friends');
            this.settingsButton = Tools.createButton(this.game,60,380,'Settings');
            this.cancelQueue = Tools.createButton(this.game,60,380,'Settings');
            this.news = null;
            this.shop = null;
            this.chat = null;

            this.messageContainer = this.window.addChild(this.add.sprite(300, 320, "messageWindow"));
            //config.interface.fontStyles.message.wordWrapWidth =  this.messageContainer.width - 50;
            this.messageContainer.alpha = 0;
            this.messageContainer.message = this.messageContainer.addChild(this.game.add.text(40, 40, ' ',config.interface.fontStyles.message));
            this.messageContainer.cancelQueueButton = this.messageContainer.addChild(Tools.createButton(this.game,300,this.messageContainer.height-85,'cancel',{sprite:"button-big-red"}));
            this.messageContainer.cancelQueueButton.events.onInputDown.add(this.leaveQueue, this);

            this.playButton.events.onInputDown.add(this.playGame, this);
            this.inventoryButton.events.onInputDown.add(this.goToInventory, this);
            this.friendsButton.events.onInputDown.add(this.goToFriends, this);
            this.settingsButton.events.onInputDown.add(this.goToSettings, this);

            /**
             * this.playGame(); //debug
             **/
            GAME.socket.update(this);
        },
        update : function (GAME){

        },
        playGame: function  () {
            this.joinQueue();
            //show popup
            //add leave queue condition
        },
        goToInventory : function () {
            this.game.state.start('inventory');
        },
        goToFriends : function () {
            //this.game.state.start('friends');
        },
        goToSettings : function () {
            //this.game.state.start('settings');
        },
        joinQueue : function () {
            this.GAME.socket.queueChannel = this.GAME.socket.subscribe("matchingQueue");
            this.GAME.socket.queueChannel.watch(function (eventObj) {
                console.log(eventObj);
            });
            this.GAME.socket.emit("joinQueue");
        },
        leaveQueue : function () {
            this.GAME.socket.unsubscribe("matchingQueue");
            this.GAME.socket.emit("leaveQueue");
            delete this.GAME.socket.queueChannel;
            this.add.tween(this.messageContainer).to( { alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
        }
    };

    window['phaser'] = window['phaser'] || {};
    window['phaser'].Home = Home;

}());