(function() {
    'use strict';
    function Preloader(GAME) {
        this.asset = null;
        this.ready = false;

    }

    Preloader.prototype = {

        preload: function (GAME) {
            this.game.plugins.add(PhaserSpine.SpinePlugin);
            this.game.add.plugin(Fabrique.Plugins.InputField);
            this.asset = this.add.sprite(320, 240, 'preloaderBar');
            this.asset.anchor.setTo(0.5, 0.5);
            this.load.setPreloadSprite(this.asset);
            this.game.load.spine('human-male', "assets/characters/human/male/human-male.json");
            this.load.onLoadComplete.addOnce(this.onLoadComplete, this);


            /** Backgrounds **/
            this.game.load.image('spaceBackground', 'assets/elements/backgrounds/spaceBackground.png');

            /** PANELS **/
            this.game.load.image('loginWindow', 'assets/elements/panels/loginWindow.png');
            this.game.load.image('window', 'assets/elements/panels/window.png');
            this.game.load.image('inventoryWindow', 'assets/elements/panels/inventoryWindow.png');
            this.game.load.image('smallWindow', 'assets/elements/panels/smallWindow.png');
            this.game.load.image('messageWindow', 'assets/elements/panels/messageWindow.png');

            /** BIG Buttons **/
            this.game.load.image('button-big-yellow', 'assets/elements/buttons/button-big-yellow.png');
            this.game.load.image('button-big-blue', 'assets/elements/buttons/button-big-blue.png');
            this.game.load.image('button-big-red', 'assets/elements/buttons/button-big-red.png');
            this.game.load.image('button-big-green', 'assets/elements/buttons/button-big-green.png');
            this.game.load.image('button-big-disable', 'assets/elements/buttons/button-big-disable.png');

            /** SMALL Buttons **/
            this.game.load.image('button-small-yellow', 'assets/elements/buttons/button-small-yellow.png');
            this.game.load.image('button-small-blue', 'assets/elements/buttons/button-small-blue.png');
            this.game.load.image('button-small-red', 'assets/elements/buttons/button-small-red.png');
            this.game.load.image('button-small-green', 'assets/elements/buttons/button-small-green.png');
            this.game.load.image('button-small-disable', 'assets/elements/buttons/button-small-disable.png');
            this.game.load.image('button-small-close', 'assets/elements/buttons/button-small-close.png');
            this.game.load.image('button-left-arrow', 'assets/elements/buttons/button-left-arrow.png');

            /** GENERAL OBJECTS **/
            this.game.load.image('usernameContainer', 'assets/elements/panels/usernameContainer.png');
            this.game.load.image('pswContainer', 'assets/elements/panels/pswContainer.png');

            this.game.load.image('squareSlot', 'assets/elements/squareSlot.png');
            this.game.load.image('roundSlot', 'assets/elements/roundSlot.png');
            this.game.load.image('roundSlotActive', 'assets/elements/roundSlotActive.png');
            this.game.load.image('squareSlotActive', 'assets/elements/squareSlotActive.png');
            this.game.load.image('box', 'assets/game-objects/box.png');
            this.game.load.image('character-platform', 'assets/game-objects/character-platform.png');

            this.game.load.image('common-armor', 'assets/characters/human/male/common/armor.png');
            this.game.load.image('common-gloves', 'assets/characters/human/male/common/gloves.png');
            this.game.load.image('common-boots', 'assets/characters/human/male/common/boots.png');
            this.game.load.image('common-sword', 'assets/characters/human/male/common/armor.png');
            this.game.load.image('common-shield', 'assets/characters/human/male/common/armor.png');
            this.game.load.image('human-male-leather-armor', 'assets/characters/human/male/leather/body.png');

            /** SKILLS **/
            this.load.image('slamIcon', 'assets/elements/skills/slamIcon.png');
            this.load.image('hookIcon', 'assets/elements/skills/hookIcon.png');
            this.load.image('bearTrapIcon', 'assets/elements/skills/bearTrapIcon.png');
            /* map */
            this.load.tilemap('test', 'assets/maps/test.json', null, Phaser.Tilemap.TILED_JSON);
            this.load.image('platforms', 'assets/maps/tileset.png');

        },

        create: function (GAME) {
            this.asset.cropEnabled = false;
        },

        update: function (GAME) {
            if (this.ready) {
                this.game.state.start('login');
                //this.game.state.start('inventory');
            }
        },

        onLoadComplete: function (GAME) {
            this.ready = true;
        }
    };

    window['phaser'] = window['phaser'] || {};
    window['phaser'].Preloader = Preloader;

}());