(function() {
    'use strict';

    function Login(GAME) {
        this.GAME = GAME;
        this.asset = null;
        this.ready = false;
    }

    Login.prototype = {
        preload: function (GAME) { },
        create: function (GAME) {
            this.GAME = this;
            this.background = this.add.sprite(0, 0, "spaceBackground");
            this.window = this.add.sprite(10, 10, "loginWindow");
            this.title = this.window.addChild(this.game.add.text(this.window.width/2, 10, 'WELCOME',config.interface.fontStyles.title));
            this.title.anchor.x = 0.5;

            this.usernameContainer = this.window.addChild(this.add.sprite(25, 180, "usernameContainer"));
            this.usernameContainer.inputField = this.usernameContainer.addChild(this.game.add.inputField(120, 40, config.interface.fontStyles.usernameField));

            this.pswContainer = this.window.addChild(this.add.sprite(25, 320, "pswContainer"));
            this.pswContainer.inputField = this.pswContainer.addChild(this.game.add.inputField(120, 40, config.interface.fontStyles.pswField));

            this.messageContainer = this.window.addChild(this.add.sprite(480, 480, "messageWindow"));
            config.interface.fontStyles.message.wordWrapWidth =  this.messageContainer.width - 50;
            this.messageContainer.alpha = 0;
            this.messageContainer.message = this.messageContainer.addChild(this.game.add.text(40, 40, ' ',config.interface.fontStyles.message));

            this.loginButton = this.window.addChild(Tools.createButton(this,135,460,"LOGIN"));
            this.loginButton.events.onInputDown.add(this.login, this);

        },
        update: function (GAME) {
            GAME.socket.update(this.GAME);
            if (GAME.socket.userData && GAME.socket.userData.status === "online") {
                this.state.start('selectCharacter');
            }
        },
        login: function() {
            var username = (this.usernameContainer.inputField.value) ? this.usernameContainer.inputField.value : window.location.search.replace("?","");
            var password = (this.pswContainer.inputField.value) ? this.pswContainer.inputField.value : window.location.search.replace("?","");
            var messageContainer = this.messageContainer;
            messageContainer.alpha = 0;
            if (password && username)
                this.GAME.socket.emit("login",{username:username,password:password});
            else  this.showMessage("Missing Credentials");
        },
        showMessage : function(message) {
            var messageContainer = this.GAME.messageContainer;
            messageContainer.message.setText(message);
            this.GAME.game.add.tween(messageContainer).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
        }
    };

    window['phaser'] = window['phaser'] || {};
    window['phaser'].Login = Login;

}());