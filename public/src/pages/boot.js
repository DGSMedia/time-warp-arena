(function () {
    'use strict';

    function Boot(GAME) {}

    Boot.prototype = {
        preload: function (GAME) {
            var game = this;
            window.Phaser.GAMES = [];
            this.load.image('preloaderBar', 'assets/preloader.gif');
            this.load.script('webfont', 'src/lib/webfont.js');
            this.load.script('gray', 'src/lib/grayFilter.js');
            GAME.config = config;
        },
        create: function (GAME) {
            this.input.maxPointers = 3;
            this.stage.disableVisibilityChange = true;
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.minWidth = this.game.width / 2;
            this.scale.minHeight = this.game.height / 2;
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
            this.stage.scale.forceLandscape = true;
            if(mobileDetect.phone()){
                this.scale.maxWidth = this.game.width * 2.5;
                this.scale.maxHeight = this.game.height * 2.5;
                this.scale.forceOrientation(true,false)
            } else {
                this.scale.minWidth = window.innerWidth/2;
                this.scale.minHeight = window.innerWidth/2 ;
                this.scale.maxWidth = window.innerWidth;
                this.scale.maxHeight = window.innerHeight;
            }
            window.WebFontConfig = {
                //  For some reason if we don't the browser cannot render the text the first time it's created.
                active: function() { game.time.events.add(Phaser.Timer.SECOND, function () { }, this); },
                google: { families: ['Ubuntu'] }
            };

            this.game.state.start('preloader');
        }
    };
    window['phaser'] = window['phaser'] || {};
    window['phaser'].Boot = Boot;

}());