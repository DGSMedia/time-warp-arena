(function () {
    'use strict';

    function Version(GAME) {}

    Version.prototype = {

        preload: function (GAME) {

        },

        create: function (GAME) {
            this.game.state.start('preloader');
        }
    };

    window['phaser'] = window['phaser'] || {};
    window['phaser'].Version = Version;

}());