(function () {
    'use strict';

    function Room(GAME) {
        this.playerX = 100;
        this.playerY = 250;
        this.players = [];
    }

    Room.prototype = {

        preload: function (GAME) {
            this.GAME = GAME;
        },

        create: function (GAME) {
            GAME.socket.emit("joinRoom",GAME.socket.room.id);
            this.background = this.add.sprite(0, 0, "spaceBackground");
            this.window = this.add.sprite(0, 0, "window");
            this.title = this.window.addChild(this.game.add.text(this.window.width/2, 20, 'GAME ROOM',config.interface.fontStyles.title));
            this.title.anchor.x = 0.5;
            this.readyButton = Tools.createButton(this,250,600,'Ready');
            this.readyButton.events.onInputDown.add(this.ready, this);
            this.backButton = Tools.createButton(this,520,600,'back');
            this.backButton.events.onInputDown.add(this.goBack, this);
            this.isReady = false;
        },
        update: function (GAME){
            GAME.socket.update(this.GAME);
            this.updateRoom();
        },
        ready : function (){
            if(this.isReady) return;
            this.GAME.socket.emit("confirmRoom",this.GAME.socket.room.id);
            this.isReady = true;
        },
        goBack : function  (){
            this.isReady = false;
            this.GAME.socket.emit("leaveRoom",this.GAME.socket.room.id);
        },
        updateRoom : function () {
            this.playersUsername = (this.GAME.socket.room && this.GAME.socket.room.players)? Object.keys(this.GAME.socket.room.players) : null;
            this.playerY = 250;
            if (!this.playersUsername) return;
            for(var p = 0; p < this.playersUsername.length; p++){
                if(this.players[p]){
                    this.players[p].destroy();
                    delete this.players[p];
                }
                if(this.playersUsername[p] === this.GAME.socket.username){
                    this.players[p] = this.game.add.text(this.playerX, this.playerY, this.playersUsername[p],{fill:"#fff000", stroke:"#000000",strokeThickness:2});
                }else {
                    this.players[p] = this.game.add.text(this.playerX, this.playerY, this.playersUsername[p],{fill:"#ffffff", stroke:"#000000",strokeThickness:2});
                }
                this.playerY = this.playerY +25;
            }
        }
    };
    window['phaser'] = window['phaser'] || {};
    window['phaser'].Room = Room;
}());