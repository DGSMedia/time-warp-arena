(function () {
    'use strict';

    function GameResult(GAME) {}

    GameResult.prototype = {

        preload: function (GAME) {
            this.GAME = GAME;
        },

        create: function (GAME) {
            this.background = this.add.sprite(0, 0, "spaceBackground");
            this.window = this.add.sprite(0, 0, "window");
            this.title = this.window.addChild(this.game.add.text(this.window.width/2, 20, 'GAME OVER',config.interface.fontStyles.title));
            this.title.anchor.x = 0.5;
            this.resultText = this.window.addChild(this.add.text(this.window.width/2, 300,"",config.interface.fontStyles.subTitle));
            this.resultText.anchor.x = 0.5;
            switch (this.GAME.room.result){
                case  "win": this.win(); break;
                case  "lose": this.lose(); break;
                case  "draw": this.draw(); break;
            }
            sessionStorage['user'] = JSON.stringify(GAME.profile);
            this.backButton = Tools.createButton(this,520,420,'close');
            this.backButton.events.onInputDown.add(this.goBack, this);
        },
        update: function (GAME){

        },
        goBack : function  (){
            this.ready = false;
            this.game.state.start('home')
        },
        win: function () {
            this.resultText.text = "Congratulations! \nyou win!";
            //this.GAME.profile.character.experience += 300;
            //this.GAME.profile.character.inventory.adena += 300;
        },
        lose: function () {
            this.resultText.text = "You lose!";
            //this.GAME.profile.character.experience += 50;
            //this.GAME.profile.character.inventory.adena += 10;
        },
        draw: function () {
            this.resultText.text = "Not so bad! \ndraw!";
           // this.GAME.profile.character.experience += 100;
            //this.GAME.profile.character.inventory.adena += 100;
        }
    };
    window['phaser'] = window['phaser'] || {};
    window['phaser'].GameResult = GameResult;
}());