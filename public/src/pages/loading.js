(function () {
    'use strict';

    function Loading(GAME) {}

    Loading.prototype = {

        preload: function (GAME) {
            this.GAME = GAME;
            /*preload*/
            this.preloadBar = this.add.sprite(320, 240, 'preloaderBar');
            this.preloadBar.anchor.setTo(0.5, 0.5);
            this.load.setPreloadSprite(this.preloadBar);
            this.load.onLoadComplete.addOnce(this.onLoadComplete, this);

            /* map */
            //this.load.tilemap('map-world-1', 'assets/maps/compressed_world.json', null, Phaser.Tilemap.TILED_JSON);
            this.load.tilemap('map-world-1', 'assets/maps/testmap.json', null, Phaser.Tilemap.TILED_JSON);
            this.load.image('world-1-base-tiles', 'assets/maps/spritesheet_antique.png');

            /* game ui */
            this.load.image('compass', 'assets/gui/touchBase.png');
            this.load.image('touch', 'assets/gui/touch.png');
            this.load.image('j', 'assets/gui/btn.png');
            this.load.image('a', 'assets/elements/skills/slamIcon.png');
            this.load.image('b', 'assets/elements/skills/hookIcon.png');
            this.load.image('c', 'assets/elements/skills/bearTrapIcon.png');

            /* game objects */
            this.load.image('bullet', 'assets/game-objects/box.png');
            this.load.image('box', 'assets/game-objects/box.png');
            this.load.image('bearTrap', 'assets/game-objects/trap.png');
            this.load.image('hook', 'assets/game-objects/hook.png');
            this.load.image('rope', 'assets/game-objects/rope.png');
            this.load.image('soul', 'assets/game-objects/soul.png');

        },

        create: function (GAME) {

        },

        update : function (GAME){
            GAME.socket.update(this.GAME);
            if(this.ready) {
                this.ready = false;
                GAME.state.start('game');
            }
        },
        onLoadComplete : function() {
            this.ready = true;
        }
    };

    window['phaser'] = window['phaser'] || {};
    window['phaser'].Loading = Loading;

}());