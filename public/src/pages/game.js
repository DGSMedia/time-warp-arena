(function () {
    'use strict';

    function Game(GAME) { }

    Game.prototype = {
        preload: function (GAME) {
            this.GAME = GAME;
            this.grayfilter = this.add.filter('Gray');
            this.roomPlayers = this.GAME.room.players;
            this.players = {};
            this.skills = [];
            this.score = {
                red: 0,
                blue:0,
                update: function (game) {
                    if(!game) return;
                    game.topMenu.teamScore.red.text = this.red;
                    game.topMenu.teamScore.blue.text = this.blue;
                }
            };
        },
        create: function (GAME) {
            this.createWorld(GAME);
            this.skills = new SKILLS();
            this.addPlayers(GAME);
            this.addInterface(GAME);
            this.addControlls(GAME);
            this.GAME.socket.emit("readyToPlay",this.GAME.socket.room.id);
        },
        update: function (GAME){
            GAME.game = this;
            GAME.socket.update(this.GAME);
            var me = this.players[GAME.socket.username];
            if (!me) return;
            me.aim = (!me.aim) ? "left" : me.aim;
            this.camera.follow(me);
            for (var playerId in this.players) {
                var player = this.players[playerId];
                this.physics.arcade.collide( player, this.game.platforms );
                for (var targetId in this.players) {
                    var target = this.players[targetId];
                    this.physics.arcade.overlap(target, player.skills.a.bullets, player.skills.a.onHit, null, this);
                    this.physics.arcade.overlap(target, player.skills.b.bullets, player.skills.b.onHit, null, this);
                    this.physics.arcade.overlap(target, player.skills.c.bullets, player.skills.c.onHit, null, this);
                }
                if (player.healthBar) player.healthBar.setPercent((player.health / player.maxHealth) *100);
            }
            this.broadCastPlayerMove(GAME.socket,me);
            this.broadCastPlayerStatus(GAME.socket,me);
            this.score.update(this);
        },
        gameOver: function (GAME) {
            //clearInterval(GAME.sync);
            this.moveControl.active = false;
            this.game.world.removeAll();
            GAME.socket.emit("getGameResult",GAME.room.id);
        },
        loadAvatarConfig : function (avatar,avatarConfig){
            var aspect = avatarConfig.aspect;
            var equip = avatarConfig.equipment;
            var config = {
                "right-arm": {"attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/right-arm" },
                "weapon": { "attachment": "weapons/"+equip.weapon.render },
                "right-hand": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.gloves.render+"/right-hand" },
                "right-leg": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/right-leg" },
                "right-feet": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.boots.render+"/right-feet" },
                "left-leg":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/left-leg" },
                "left-feet":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.boots.render+"/left-feet" },
                "body": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/body" },
                "head": { "attachment": aspect.race+"/"+aspect.sex+"/shared/head" },
                "beard":{"attachment": aspect.race+"/"+aspect.sex+"/beard/"+aspect.details, color:aspect.hairColor },
                "mouth":{"attachment": aspect.race+"/"+aspect.sex+"/shared/mouth-fight" },
                "eyes":{"attachment": aspect.race+"/"+aspect.sex+"/shared/eyes-open" },
                "eyebrows":{"attachment": aspect.race+"/"+aspect.sex+"/shared/eyebrows", color:aspect.hairColor },
                "hair":{"attachment": aspect.race+"/"+aspect.sex+"/hairs/"+aspect.hairStyle, color:aspect.hairColor},
                "helmet":{"attachment": aspect.race+"/"+aspect.sex+"/"+equip.helmet.render+"/helmet"},
                "back-off-hand":{"attachment": "weapons/"+equip.offHand.render },
                "left-arm":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/left-arm" },
                "left-hand":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.gloves.render+"/left-hand" },
                "front-off-hand":{ "attachment": "weapons/"+equip.shield.render }
            };
            var partIndex = 0;
            for (var part in config){
                var attachmentName = config[part].attachment;
                if(!avatar.skeleton.findSlot(part))  continue;
                if(attachmentName){
                    avatar.skeleton.findSlot(part).attachment = avatar.skeleton.data.skins[0].attachments[partIndex+":"+attachmentName];
                }
                partIndex++;
                if(config[part].color) {
                    var rgbColor = config[part].color;
                    avatar.skeleton.findSlot(part).r = rgbColor.red/255;
                    avatar.skeleton.findSlot(part).g = rgbColor.green/255;
                    avatar.skeleton.findSlot(part).b = rgbColor.blue/255;
                }
            }
        },
        createWorld: function (GAME) {
            this.world.setBounds(0, 0, 1920, 440);
            this.physics.startSystem(Phaser.Physics.ARCADE);
            this.physics.arcade.checkCollision.down = false;
            this.game.platforms = addMap(this).layer;
            function addMap(game) {
                /*
                var map = game.game.add.tilemap('map-world-1');
                map.addTilesetImage('spritesheet_antique', 'world-1-base-tiles');
                map.setCollisionBetween(231, 232);
                map.setCollisionBetween(212, 213);
                map.forEach (function(tile) {
                    if(tile.index === 213 || tile.index === 213) {
                        tile.collideDown = false;
                        tile.collideLeft = false;
                        tile.collideRight = false;
                    }
                });
                var layer = map.createLayer('base_layer');
                layer.resizeWorld();
                */
                var map = game.add.tilemap('test');
                map.addTilesetImage('tileset', 'platforms');
                map.setCollisionBetween(0,255);
                map.forEach (function(tile) {
                    if([20,21,51,52,53].indexOf(tile.index) >= 0) {
                        console.log(tile.index);
                        tile.collideDown = false;
                        tile.collideLeft = false;
                        tile.collideRight = false;
                    }
                });
                var  layer = map.createLayer('platforms');
                var  layer2 = map.createLayer('objects');
                layer.resizeWorld();
                layer2.resizeWorld();

                return {
                    layer:layer,
                    layer2:layer2
                };
            }
        },
        addPlayers : function (GAME) {
            for (var username in this.roomPlayers){
                this.players[username] = new PLAYER(this,username,this.roomPlayers[username].team);
            }
            this.players[GAME.socket.username].bringToTop();
        },
        addControlls: function (GAME){
            var player = this.players[GAME.socket.username];
            this.moveControl = this.game.plugins.add(Phaser.Plugin.TouchControl);
            this.moveControl.inputEnable();
            this.buttonJ = this.game.add.button(0, 0, 'j', function(btn){playerJump(this,btn)}, this, 2, 1, 0);
            this.buttonA = this.game.add.button(0, 0, 'a', function(btn){skill(this,btn)}, this, 2, 1, 0);
            this.buttonB = this.game.add.button(0, 0, 'b', function(btn){skill(this,btn)}, this, 2, 1, 0);
            this.buttonC = this.game.add.button(0, 0, 'c', function(btn){skill(this,btn)}, this, 2, 1, 0);

            this.buttonJ.fixedToCamera = true; this.buttonJ.cameraOffset.x = 1180; this.buttonJ.cameraOffset.y = 630; this.buttonJ.anchor.set(0.5);
            this.buttonA.fixedToCamera = true; this.buttonA.cameraOffset.x = 1030; this.buttonA.cameraOffset.y = 645; this.buttonA.anchor.set(0.5);
            this.buttonB.fixedToCamera = true; this.buttonB.cameraOffset.x = 1080; this.buttonB.cameraOffset.y = 530; this.buttonB.anchor.set(0.5);
            this.buttonC.fixedToCamera = true; this.buttonC.cameraOffset.x = 1200; this.buttonC.cameraOffset.y = 490; this.buttonC.anchor.set(0.5);


            function playerJump(game,btn) {
                if(!game.moveControl.active) return;
                var now = Math.floor(Date.now() / 100);
                var socket = game.GAME.socket;
                var eventObject = {
                    event:"playerMove",
                    room : GAME.room.id,
                    data:{ x:player.x, y:player.y, health:player.health },
                    sender: player.username,
                    action:"jump"
                };
                if ( player && player.alive && player.body.onFloor() ){
                    if (socket.lastMove === eventObject.action)  return;
                    if (!socket.lastCastTime) socket.lastCastTime = now;
                    if (!socket.lastMove && eventObject.action) socket.lastMove = eventObject.action;
                    player.updatePosition(eventObject);
                    if (eventObject.action && !player.onAction) socket.emit("broadCastPlayerMove", eventObject);
                    socket.lastCastTime = now;
                    if(eventObject.action)socket.lastMove = eventObject.action;
                }

            }
            function skill(game,btn) {
                if(!game.moveControl.active) return;
                if (player.onAction) return;
                var eventObject = {
                    event:"playerSkill",
                    room : game.GAME.room.id,
                    data:{
                        x:player.x,
                        y:player.y,
                        health:player.health,
                        skillIndex:btn.key
                    },
                    sender: player.username
                };
                if (player && player.alive) game.GAME.socket.emit("broadCastPlayerSkill", eventObject);
                btn.alpha = 0.3;
                btn.tint = 0xe5e5e5;
                btn.inputEnabled = false;
                var buttonRecycle = game.add.tween(btn).to( { alpha: 1 }, player.skills[btn.key].fireRate, "Linear", true);
                buttonRecycle.onComplete.addOnce( function(){btn.inputEnabled = true; btn.tint = 0xffffff;} , game);
            }
        },
        addInterface: function (GAME){
            this.topMenu = this.add.sprite(1180, 50, null);
            this.topMenu.fixedToCamera = true;
            addTimer(this);
            addScore(this);

            function addTimer (game) {
                game.topMenu.time = Tools.createText(0,0,{fill:"#ffffff",size:30,strokeThickness:3}, game, "00 : 00");
                var time = game.topMenu.time;


                /*
                time.countDown = new CountDownTimer(config.world.time,1000);
                time.countDown.onTick(function(min,sec){
                    time.countDown.current = min+" : "+ ((sec<10)?"0"+sec : sec);
                    time.text = time.countDown.current;
                    if ( min === 0 && sec === 0 ) game.gameOver(GAME);
                });
                */
                game.topMenu.addChild(time);
            }
            function addMenu () { }

            function addScore (game) {
                game.topMenu.teamScore = game.topMenu.addChild(game.add.sprite(-150, 0, null));
                game.topMenu.teamScore.red = game.topMenu.teamScore.addChild(Tools.createText(0,0,{fill:"#ff0000",size:30,strokeThickness:3},game,game.score.red));
                game.topMenu.teamScore.blue = game.topMenu.teamScore.addChild(Tools.createText(50,0,{fill:"#0000ff",size:30,strokeThickness:3},game,game.score.red));
            }
            function addStatus () { }
        },
        broadCastPlayerMove : function (socket,player) {
            if (!player) return;
            var cursor = this.moveControl.cursors;
            var room = this.GAME.room;
            var now = Math.floor(Date.now() / 100);

            var eventObject = {
                event:"playerMove",
                room: room.id,
                data:{
                    x:player.x,
                    y:player.y,
                    health:player.health
                },
                sender: player.username
            };

            if (player.alive && cursor.left) eventObject.action = "run-left";
            else if (player.alive && cursor.right) eventObject.action = "run-right";
            else if (player.alive && player.body.onFloor()) eventObject.action = "idle";

            if (socket.lastMove === eventObject.action)  return;
            if (!socket.lastCastTime) socket.lastCastTime = now;
            if (!socket.lastMove && eventObject.action) socket.lastMove = eventObject.action;
            player.updatePosition(eventObject);
            if (eventObject.action && !player.onAction) socket.emit("broadCastPlayerMove", eventObject);
            socket.lastCastTime = now;
            if(eventObject.action) socket.lastMove = eventObject.action;


        },
        broadCastPlayerStatus : function (socket,player) {
            if (!player) return;
            var room = this.GAME.room;
            var eventObject = {
                event:"playerStatus",
                room: room.id,
                data:{
                    x:player.x,
                    y:player.y,
                    health:player.health
                },
                sender: player.username
            };
            var isPlayerDead = !player.alive && !player.dead;
            if(isPlayerDead){
                player.dead = true;
                eventObject.data.status = "dead";
                socket.emit("broadCastPlayerStatus", eventObject)
            }
        },
        updateScore : function (ownerId,target,game){
            var owner = game.players[ownerId];
            if (target.health > 0) return;
            game.score[owner.team] = game.score[owner.team]+1;
            var params = { room : game.GAME.room.id, data: game.score, event:"updateScore" };
            game.GAME.socket.emit("updateScore",params);
        },
        calcDamage : function (power,rate,multiplier){
            multiplier = (!multiplier)? 1.5 : multiplier;
            rate = (rate > 100)? 100 : rate;
            var isCritical =  Math.floor(Math.random() * 100) + 1 <= rate;
            return (isCritical)? parseInt(power*multiplier)+"!" :power;
        },
        showDamageText : function (game,text,x,y) {
            var fontStyle = (text.toString().indexOf("!") < 0)? config.interface.fontStyles.textDamage : config.interface.fontStyles.textDamageCrit;
            var textSprite = game.add.text(x, y-100, text,fontStyle);
            var textTween = game.add.tween(textSprite).to( { alpha: 0,y :y -200 }, 1500, "Linear", true);
            textTween.onComplete.addOnce( function() { textSprite.destroy() }, game);
        },
        playerHitted : function (target) {
            if (target.username === this.GAME.socket.username && target.health < target.maxHealth/6) this.camera.flash(0xff0000, 200);
            target.avatar.setAnimationByName(1,"hitted",false);
            target.alpha = 0;
            var flashTween = this.add.tween(target).to( { alpha: 1 }, 250, "Linear", true);
            flashTween.repeat(1,150);
        }
    };
    window['phaser'] = window['phaser'] || {};
    window['phaser'].Game = Game;
}());