(function () {
    'use strict';

    function Inventory(GAME) {
        this.GAME = GAME;
    }

    Inventory.prototype = {

        preload: function (GAME) {
            this.GAME = GAME;
            this.background = this.add.sprite(0, 0, "spaceBackground");
            this.window = this.add.sprite(0, 0, "inventoryWindow");
            this.title = this.window.addChild(this.game.add.text(this.window.width/2, 20, 'INVENTORY',config.interface.fontStyles.title));
            this.title.anchor.x = 0.5;
        },
        create: function (GAME){
            var userData = GAME.socket.userData;
            var slotData = userData.slots[userData.slotIndex];
            this.menu  = this.addSubMenu();

            this.previewBox = this.addPreview(50,200,slotData);
            this.previewSkillBox = this.addPreviewSkill();
            this.equipBox = this.addEquipSlots();
            this.skillsBox = this.addSkillSlots();
            this.updatePreviewSlots(this.previewBox.equip,slotData.inventory.equip);
            this.updateSkillsPreviwSlots(this.previewSkillBox.slots,slotData.inventory.skills);
            this.updateEquipSlots(this.equipBox.children,slotData.inventory.equip);
            this.updateSkillSlots(this.skillsBox.children,slotData.inventory.skills);

            this.backButton = this.window.addChild(Tools.createButton(this,1150,50,'',{sprite:"button-small-close"}));
            this.backButton.events.onInputDown.add(this.goBack, this);
            GAME.socket.update(this);
        },
        update: function (GAME){
            var userInfo = GAME.socket.userData;
            var slotData = userInfo.slots[userInfo.slotIndex];
            //var inventorySkills = GAME.profile.character.inventory.skills;
            //var inventoryItems = slotData.inventory.equip;
            //this.updateSkillSlots(this.skillsBox,inventorySkills);
            //this.updateEquipSlots(this.equipBox,inventoryItems);
        },
        addPreview: function (x,y,config) {
            var previewBox = this.add.group();
            previewBox.x = x;
            previewBox.y = y;
            previewBox.equip = previewBox.createMultiple(6, 'squareSlot', [0], true);
            previewBox.equip.forEach(function(child,index){
                if (index === 0) previewBox.equip.helmet = child;
                if (index === 1) previewBox.equip.gloves = child;
                if (index === 2) previewBox.equip.armor = child;
                if (index === 3) previewBox.equip.boots = child;
                if (index === 4) previewBox.equip.weapon = child;
                if (index === 5) previewBox.equip.offHand = child;
            });
            previewBox.align(2, -1, 400, 110);
            previewBox.avatar = previewBox.addChild(this.addAvatar(this,config));
            previewBox.avatar.x = 230;
            previewBox.avatar.y = 250;
            return previewBox
        },
        addPreviewSkill : function (){
            var previewSkills = this.add.group();
            previewSkills.visible = false;
            previewSkills.x = 100;
            previewSkills.y = 200;
            previewSkills.slots = previewSkills.createMultiple(3, 'roundSlotActive', [0], true);
            previewSkills.align(1, -1, 120, 120);
            previewSkills.slots.forEach(function(child,index) {
                if (index === 0) previewSkills.slots.A = child;
                if (index === 1) previewSkills.slots.B = child;
                if (index === 2) previewSkills.slots.C = child;
            });
            return previewSkills;
        },
        addEquipSlots: function (){
            var group = this.add.group();
            group.x = 650;
            group.y = 200;
            group.createMultiple(15, 'squareSlot', [0], true);
            group.align(5, -1, 110, 110);
            return group;
        },
        addSkillSlots: function (){
            var group = this.add.group();
            group.x = 650;
            group.y = 200;
            group.createMultiple(15, 'roundSlot', [0], true);
            group.align(5, -1, 110, 110);
            group.visible = false;
            return group;
        },
        updatePreviewSlots : function (slots, items){
            for(var i =0; i < items.length; i++){
                var item = items[i];
                if(!item.bind) continue;
                if(slots[item.type] && slots[item.type].content) slots[item.type].content.destroy();
                if(item.type === "shield") slots[item.type] = slots["offHand"];
                slots[item.type].content = slots[item.type].addChild(this.game.add.sprite(10,10,item.icon));
            }
        },
        updateSkillsPreviwSlots: function (slots, skills) {
            for (var s = 0; s < skills.length; s++) {
                var skill = skills[s];
                console.log(skill,slots[skill.bind]);
                if (!skill.bind) continue;
                slots[skill.bind].icon = slots[skill.bind].addChild(this.game.add.sprite(10,10,skill.icon));
                slots[skill.bind].description = slots[skill.bind].addChild(this.game.add.text(150,10,skill.name.toUpperCase(),config.interface.fontStyles.inventoryLabel));
                slots[skill.bind].icon.scale.x = 0.7;
                slots[skill.bind].icon.scale.y = 0.7;
                slots[skill.bind].addChild(this.game.add.text(65, 60, skill.bind.toUpperCase(),config.interface.fontStyles.inventoryLabel));
            }
        },
        updateEquipSlots : function (slots, items){
            for(var i =0; i < items.length; i++){
                var item = items[i];
                if(item.bind) continue;
                if(slots[i] && slots[i].content) slots[i].content.destroy();
                slots[i].content = slots[i].addChild(this.game.add.sprite(10,10,item.icon));

            }
        },
        updateSkillSlots : function (slots, skills){
            for(var i =0; i < skills.length; i++){
                var skill = skills[i];
                //if(skill.bind) continue;
                if (slots[i] && slots[i].content) slots[i].content.destroy();
                slots[i].content = slots[i].addChild(this.game.add.sprite(10,10,skill.icon));
                slots[i].content.scale.x = 0.7;
                slots[i].content.scale.y = 0.7;
            }
        },
        addSubMenu: function (){
            var menu  = this.add.group();
            menu.x = 80;
            menu.y = 85;
            menu.equip = menu.addChild(Tools.createButton(this,0,0,'EQUIP',{sprite:"button-big-yellow"}));
            menu.skills = menu.addChild(Tools.createButton(this,220,0,'SKILLS'));
            menu.mats = menu.addChild(Tools.createButton(this,440,0,'ITEMS',{sprite:"button-big-disable"}));
            menu.items = menu.addChild(Tools.createButton(this,660,0,'CRAFT',{sprite:"button-big-disable"}));
            menu.equip.events.onInputDown.add(this.showItems, this);
            menu.skills.events.onInputDown.add(this.showSkills, this);
            return menu;
        },
        showItems: function(button){
            this.previewSkillBox.visible = false;
            this.skillsBox.visible = false;
            this.menu.equip.loadTexture("button-big-blue", 0);
            this.menu.skills.loadTexture("button-big-blue", 0);
            button.loadTexture("button-big-yellow", 0);
            this.equipBox.visible = true;
            this.previewBox.visible = true;
        },
        showSkills: function(button){
            this.previewBox.visible = false;
            this.equipBox.visible = false;
            this.menu.equip.loadTexture("button-big-blue", 0);
            this.menu.skills.loadTexture("button-big-blue", 0);
            button.loadTexture("button-big-yellow", 0);
            this.skillsBox.visible = true;
            this.previewSkillBox.visible = true;
        },

        addAvatar : function(GAME,characterConfig) {
            var avatar = this.game.add.spine(0, 0, characterConfig.avatar.aspect.race+"-"+characterConfig.avatar.aspect.sex);
            avatar.setToSetupPose();
            this.loadAvatarConfig(avatar,characterConfig.avatar);
            avatar.setAnimationByName(0, "idle", true);
            avatar.scale.x = 1.5;
            avatar.scale.y = 1.5;
            return avatar;
        },
        loadAvatarConfig : function(avatar,avatarConfig){
            var aspect = avatarConfig.aspect;
            var equip = avatarConfig.equipment;
            var config = {
                "right-arm": {"attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/right-arm" },
                "weapon": { "attachment": "weapons/"+equip.weapon.render },
                "right-hand": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.gloves.render+"/right-hand" },
                "right-leg": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/right-leg" },
                "right-feet": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.boots.render+"/right-feet" },
                "left-leg":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/left-leg" },
                "left-feet":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.boots.render+"/left-feet" },
                "body": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/body" },
                "head": { "attachment": aspect.race+"/"+aspect.sex+"/shared/head" },
                "beard":{"attachment": aspect.race+"/"+aspect.sex+"/beard/"+aspect.details, color:aspect.hairColor },
                "mouth":{"attachment": aspect.race+"/"+aspect.sex+"/shared/mouth-fight" },
                "eyes":{"attachment": aspect.race+"/"+aspect.sex+"/shared/eyes-open" },
                "eyebrows":{"attachment": aspect.race+"/"+aspect.sex+"/shared/eyebrows", color:aspect.hairColor },
                "hair":{"attachment": aspect.race+"/"+aspect.sex+"/hairs/"+aspect.hairStyle, color:aspect.hairColor},
                "helmet":{"attachment": aspect.race+"/"+aspect.sex+"/"+equip.helmet.render+"/helmet"},
                "back-off-hand":{"attachment": "weapons/"+equip.offHand.render },
                "left-arm":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/left-arm" },
                "left-hand":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.gloves.render+"/left-hand" },
                "front-off-hand":{ "attachment": "weapons/"+equip.shield.render }
            };
            var partIndex = 0;
            for (var part in config){
                var attachmentName = config[part].attachment;
                if(!avatar.skeleton.findSlot(part))  continue;
                if(attachmentName){
                    avatar.skeleton.findSlot(part).attachment = avatar.skeleton.data.skins[0].attachments[partIndex+":"+attachmentName];
                }
                partIndex++;
                if(config[part].color) {
                    var rgbColor = config[part].color;
                    avatar.skeleton.findSlot(part).r = rgbColor.red/255;
                    avatar.skeleton.findSlot(part).g = rgbColor.green/255;
                    avatar.skeleton.findSlot(part).b = rgbColor.blue/255;
                }
            }
        },
        goBack : function(){
            this.game.state.start('home');
        }
    };

    window['phaser'] = window['phaser'] || {};
    window['phaser'].Inventory = Inventory;

}());