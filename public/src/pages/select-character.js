(function () {
    'use strict';

    function selectCharacter(GAME) {}

    selectCharacter.prototype = {

        preload: function (GAME) {
            this.GAME = GAME;
            this.slots = [];
        },
        create: function (GAME) {
            window.isOldSlots = true;
            this.background = this.add.sprite(0, 0, "spaceBackground");
            this.window = this.add.sprite(0, 0, "window");
            this.title = this.window.addChild(this.game.add.text(this.window.width/2, 20, 'CHARACTER SELECTION',config.interface.fontStyles.title));
            this.title.anchor.x = 0.5;
            this.createSlots(GAME);


            /**  DEBUG
            this.GAME.socket.userData.slotIndex = 0;
            this.GAME.socket.emit("selectSlot",0);
            this.game.state.start('home');
            **/
            // load players configurations
            // fill slots
            // add buttons to slots [create] [select] [delete]
        },
        update: function(GAME) {
            GAME.socket.update(this.GAME);
            if(window.isOldSlots) {
                this.updateSlots(GAME);
            }
        },
        createSlots : function(GAME){
            var profileData = GAME.socket.userData;
            var posX = 0;
            this.slots = [];
            for(var i =0; i < 3; i++){
                if(this.slots[i])this.slots[i].destroy();
                var slot = profileData.slots[i];
                this.slots.push(this.add.sprite(posX+250, 400, null));
                if (slot && window.isOldSlots) {
                    var experience = Tools.getLevel(slot.experience);
                    this.slots[i].platform = this.slots[i].addChild(this.add.sprite(-100,-35,"character-platform"));
                    this.slots[i].textLevelLabel = this.slots[i].addChild(Tools.createText(-70, 50, {fill : "#ffcc00", font : "20px Ubuntu", strokeThickness : 3 }, this, "Level :"));
                    this.slots[i].textLevelValue = this.slots[i].addChild(Tools.createText(0, 50, {fill : "#ffffff", font : "20px Ubuntu", strokeThickness : 3 }, this, experience.level));
                    this.slots[i].textRankLabel = this.slots[i].addChild(Tools.createText(-70, 80, {fill : "#ffcc00", font : "20px Ubuntu", strokeThickness : 3 }, this, "Rank :"));
                    this.slots[i].textRankValue = this.slots[i].addChild(Tools.createText(0, 80, {fill : "#ffffff", font : "20px Ubuntu", strokeThickness : 3 }, this, slot.rank));
                    this.slots[i].textExp = this.slots[i].addChild(Tools.createText(90, 50, {fill : "#ffffff", font : "20px Ubuntu", strokeThickness : 3 }, this, experience.levelExp+" / "+experience.maxExp));
                    this.slots[i].btnSelect = this.slots[i].addChild(Tools.createButton(this,-100,120,"SELECT",{sprite:"button-big-yellow"}));
                    this.slots[i].btnDelete = this.slots[i].addChild(Tools.createButton(this,-100,200,"DELETE",{sprite:"button-big-red"}));
                    this.slots[i].btnSelect.id = i;
                    this.slots[i].btnDelete.id = i;
                    this.slots[i].btnSelect.events.onInputDown.add(this.selectCharacter, this);
                    this.slots[i].btnDelete.events.onInputDown.add(this.deleteCharacter, this);
                    this.slots[i].avatar = this.slots[i].addChild(this.addAvatar(this,profileData.slots[i]));
                } else {
                    this.slots[i].btnCreate = this.slots[i].addChild(Tools.createButton(this,-100,200,"CREATE"));
                    this.slots[i].btnCreate.id = i;
                    this.slots[i].btnCreate.events.onInputDown.add(this.goToCreateCharacter, this);
                }
                posX = posX+350;
            }
        },
        updateSlots : function (GAME){
            for(var i =0; i < 3; i++) {
                if(this.slots[i])this.slots[i].destroy();
            }
            this.createSlots(GAME);
            window.isOldSlots = false;
        },
        goToCreateCharacter: function (button) {
            this.game.state.start('createCharacter');
        },
        selectCharacter: function(button){
            var slotIndex = button.id;
            this.GAME.socket.userData.slotIndex = slotIndex;
            this.GAME.socket.emit("selectSlot",slotIndex);
            this.game.state.start('home');
        },
        deleteCharacter: function(button){
            //add confirmation
            this.GAME.socket.emit("deleteSlot",{ index:button.id });
        },
        addAvatar : function(GAME,characterConfig) {
            var avatar = this.game.add.spine(0, 0, characterConfig.avatar.aspect.race+"-"+characterConfig.avatar.aspect.sex);
            avatar.setToSetupPose();
            this.loadAvatarConfig(avatar,characterConfig.avatar);
            avatar.setAnimationByName(0, "idle", true);
            avatar.scale.x = 1.5;
            avatar.scale.y = 1.5;
            return avatar;
        },
        loadAvatarConfig : function(avatar,avatarConfig){
            var aspect = avatarConfig.aspect;
            var equip = avatarConfig.equipment;
            var config = {
                "right-arm": {"attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/right-arm" },
                "weapon": { "attachment": "weapons/"+equip.weapon.render },
                "right-hand": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.gloves.render+"/right-hand" },
                "right-leg": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/right-leg" },
                "right-feet": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.boots.render+"/right-feet" },
                "left-leg":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/left-leg" },
                "left-feet":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.boots.render+"/left-feet" },
                "body": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/body" },
                "head": { "attachment": aspect.race+"/"+aspect.sex+"/shared/head" },
                "beard":{"attachment": aspect.race+"/"+aspect.sex+"/beard/"+aspect.details, color:aspect.hairColor },
                "mouth":{"attachment": aspect.race+"/"+aspect.sex+"/shared/mouth-fight" },
                "eyes":{"attachment": aspect.race+"/"+aspect.sex+"/shared/eyes-open" },
                "eyebrows":{"attachment": aspect.race+"/"+aspect.sex+"/shared/eyebrows", color:aspect.hairColor },
                "hair":{"attachment": aspect.race+"/"+aspect.sex+"/hairs/"+aspect.hairStyle, color:aspect.hairColor},
                "helmet":{"attachment": aspect.race+"/"+aspect.sex+"/"+equip.helmet.render+"/helmet"},
                "back-off-hand":{"attachment": "weapons/"+equip.offHand.render },
                "left-arm":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/left-arm" },
                "left-hand":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.gloves.render+"/left-hand" },
                "front-off-hand":{ "attachment": "weapons/"+equip.shield.render }
            };
            var partIndex = 0;
            for (var part in config){
                var attachmentName = config[part].attachment;
                if(!avatar.skeleton.findSlot(part))  continue;
                if(attachmentName){
                    avatar.skeleton.findSlot(part).attachment = avatar.skeleton.data.skins[0].attachments[partIndex+":"+attachmentName];
                }
                partIndex++;
                if(config[part].color) {
                    var rgbColor = config[part].color;
                    avatar.skeleton.findSlot(part).r = rgbColor.red/255;
                    avatar.skeleton.findSlot(part).g = rgbColor.green/255;
                    avatar.skeleton.findSlot(part).b = rgbColor.blue/255;
                }
            }
        }
    };
    window['phaser'] = window['phaser'] || {};
    window['phaser'].selectCharacter = selectCharacter;

}());