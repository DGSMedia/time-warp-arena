(function () {
    'use strict';

    function createCharacter(GAME) {}

    createCharacter.prototype = {

        preload: function (GAME) {
            this.GAME = GAME;
            this.customisationsAvailable = config.customisationsAvailable;
            this.defaultConfig = {
                    aspect : {
                        race : "human",
                        sex : "male",
                        hairStyle : "hair-style-one",
                        hairColor : { red:0, green:150, blue:51 },
                        details : "beard-style-one",
                        detailsColor : { red:0, green:150, blue:51 }
                    },
                    equipment : {
                        "weapon" : {
                            name : "common sword",
                            type : "weapon",
                            class : "sword",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-sword",
                            render : "common-sword"
                        },
                        "helmet": {},
                        "armor" : {
                            name : "common armor",
                            type : "armor",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "human-male-common-armor",
                            render : "common"
                        },
                        "gloves" : {
                            name : "common gloves",
                            type : "gloves",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-gloves",
                            render : "common"
                        },
                        "boots" : {
                            name : "common boots",
                            type : "boots",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-boots",
                            render : "common"
                        },
                        "shield" : {
                            name : "common shield",
                            type : "shield",
                            status : 101,
                            bind : true,
                            attributes : { str:1, dex:1, int:1 },
                            icon : "common-shield",
                            render : "common-shield"
                        },
                        "offHand": {}
                    }
                };
        },
        create: function (GAME) {
            this.background = this.add.sprite(0, 0, "spaceBackground");
            this.window = this.add.sprite(0, 0, "window");
            this.title = this.window.addChild(this.game.add.text(this.window.width/2, 20, 'CREATE NEW CHARACTER',config.interface.fontStyles.title));
            this.title.anchor.x = 0.5;
            this.addPreview(70,180,this.defaultConfig);
            this.addConfigurator(500,180);
            this.addActionBox(750,580);
        },
        addPreview: function (x,y,defaultConfig) {
            this.previewBox = Tools.createBox(this,x,y,350,350);
            this.platform = this.previewBox.addChild(this.add.sprite(35,270,"character-platform"));
            this.platform.scale.x = 2;
            this.platform.scale.y = 2;
            this.avatar = this.previewBox.addChild(this.addAvatar(this,defaultConfig));
        },
        addConfigurator: function(x,y) {
            this.configBox = Tools.createBox(this,x,y,700,350);
            /** RACE **/
            this.raceLabel = this.configBox.addChild(Tools.createText(100, 40, config.interface.fontStyles.creationLabel, this, "Race"));
            this.raceSelector = this.configBox.addChild(Tools.createButton(this,200,15," Loading...",{sprite:"button-big-disable"}));
            this.raceSelector.events.onInputDown.add(this.getRace, this);
            /** SEX **/
            this.sexSelector = this.configBox.addChild(Tools.createButton(this,450,15," Loading...",{sprite:"button-small-disable"}));
            this.sexSelector.events.onInputDown.add(this.getSex, this);

            /** COLOR **/
            this.colorLabel = this.configBox.addChild(Tools.createText(100, 110, config.interface.fontStyles.creationLabel, this, "Color"));
            this.colorSelector = this.configBox.addChild(Tools.createButton(this,200,90," Loading...",{sprite:"button-small-green"}));
            this.colorSelector.events.onInputDown.add(this.getColor, this);

            /** Style **/
            this.styleLabel = this.configBox.addChild(Tools.createText(100, 180, config.interface.fontStyles.creationLabel, this, "Style"));
            this.styleSelector = this.configBox.addChild(Tools.createButton(this,200,160," Loading...",{sprite:"button-small-green"}));
            this.styleSelector.events.onInputDown.add(this.getStyle, this);

            /** Details **/
            this.datailLabel = this.configBox.addChild(Tools.createText(100, 250, config.interface.fontStyles.creationLabel, this, "Details"));
            this.datailSelector = this.configBox.addChild(Tools.createButton(this,200,230," Loading...",{sprite:"button-small-green"}));
            this.datailSelector.events.onInputDown.add(this.getDetails, this);

            this.getRace(this.raceSelector);
            this.getSex(this.sexSelector);
            this.getColor(this.colorSelector);
            this.getStyle(this.styleSelector);
            this.getDetails(this.datailSelector);
        },
        addActionBox : function (x,y) {
            this.actionBox = Tools.createBox(this,x,y,480,100);
            this.backButton = this.actionBox.addChild(Tools.createButton(this,20,20,'BACK'));
            this.createButton = this.actionBox.addChild(Tools.createButton(this,250,20,'CREATE'));
            this.backButton.events.onInputDown.add(this.goBack, this);
            this.createButton.events.onInputDown.add(this.createCharacter, this);
        },
        goBack : function () {
            this.game.state.start('selectCharacter');
        },
        createCharacter : function () {
            var userData = this.GAME.socket.userData;
            if (userData.slots.length > 2 ){  this.game.state.start('selectCharacter'); return  }
            this.saveSlot(this.defaultConfig,userData.slots.length);
            this.game.state.start('selectCharacter');
        },
        getRace: function (selector) {
            var races = Object.keys(this.customisationsAvailable);
            selector.indexValue = (!selector.indexValue && selector.indexValue!==0 )? 0:selector.indexValue+1;
            if(selector.indexValue > races.length-1) selector.indexValue = 0;
            selector.children[0].setText(races[selector.indexValue].toUpperCase());
            selector.value =  races[selector.indexValue];
            this.defaultConfig.aspect.race = selector.value;
        },
        getSex: function (selector) {
            var raceValue = this.raceSelector.value;
            var sexs = Object.keys(this.customisationsAvailable[raceValue].sex);
            selector.indexValue = (!selector.indexValue && selector.indexValue!==0 )? 0:selector.indexValue+1;
            if(selector.indexValue > sexs.length-1) selector.indexValue = 0;
            var sexText = (sexs[selector.indexValue] === "male")?  "♂": "♀";
            selector.children[0].setText(sexText);
            selector.children[0].style.fontSize = 45;
            selector.value = sexs[selector.indexValue];
            this.defaultConfig.aspect.sex = selector.value;
        },
        getColor : function (selector) {
            var raceValue = this.raceSelector.value;
            var sexValue = this.sexSelector.value;
            var colors = this.customisationsAvailable[raceValue].sex[sexValue].color;
            selector.indexValue = (!selector.indexValue && selector.indexValue!==0 )? 0:selector.indexValue+1;
            if(selector.indexValue > colors.length-1) selector.indexValue = 0;
            selector.children[0].setText(selector.indexValue+1);
            this.defaultConfig.aspect.hairColor = colors[selector.indexValue];
        },
        getStyle: function (selector) {
            var raceValue = this.raceSelector.value;
            var sexValue = this.sexSelector.value;
            var styles = this.customisationsAvailable[raceValue].sex[sexValue].style;
            selector.value = (!selector.value && selector.value!==0 )? 0:selector.value+1;
            if(selector.value > styles.length-1) selector.value = 0;
            selector.children[0].setText(selector.value+1);
            this.defaultConfig.aspect.hairStyle = styles[selector.value];
        },
        getDetails: function (selector) {
            var raceValue = this.raceSelector.value;
            var sexValue = this.sexSelector.value;
            var details = this.customisationsAvailable[raceValue].sex[sexValue].details;
            selector.value = (!selector.value && selector.value!==0 )? 0:selector.value+1;
            if(selector.value > details.length-1) selector.value = 0;
            selector.children[0].setText(selector.value+1);
            this.defaultConfig.aspect.details = details[selector.value];
        },
        addAvatar : function (GAME,characterConfig) {
            var avatar = this.game.add.spine(0, 0, characterConfig.aspect.race+"-"+characterConfig.aspect.sex);
            avatar.setToSetupPose();
            this.loadAvatarConfig(avatar,characterConfig);
            avatar.setAnimationByName(0, "idle", true);
            avatar.scale.x = 2;
            avatar.scale.y = 2;
            avatar.x = 230;
            avatar.y = 320;
            return avatar;
        },
        loadAvatarConfig : function (avatar,avatarConfig){
            var aspect = avatarConfig.aspect;
            var equip = avatarConfig.equipment;
            var config = {
                "right-arm": {"attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/right-arm" },
                "weapon": { "attachment": "weapons/"+equip.weapon.render },
                "right-hand": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.gloves.render+"/right-hand" },
                "right-leg": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/right-leg" },
                "right-feet": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.boots.render+"/right-feet" },
                "left-leg":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/left-leg" },
                "left-feet":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.boots.render+"/left-feet" },
                "body": { "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/body" },
                "head": { "attachment": aspect.race+"/"+aspect.sex+"/shared/head" },
                "beard":{"attachment": aspect.race+"/"+aspect.sex+"/beard/"+aspect.details, color:aspect.hairColor },
                "mouth":{"attachment": aspect.race+"/"+aspect.sex+"/shared/mouth-fight" },
                "eyes":{"attachment": aspect.race+"/"+aspect.sex+"/shared/eyes-open" },
                "eyebrows":{"attachment": aspect.race+"/"+aspect.sex+"/shared/eyebrows", color:aspect.hairColor },
                "hair":{"attachment": aspect.race+"/"+aspect.sex+"/hairs/"+aspect.hairStyle, color:aspect.hairColor},
                "helmet":{"attachment": aspect.race+"/"+aspect.sex+"/"+equip.helmet.render+"/helmet"},
                "back-off-hand":{"attachment": "weapons/"+equip.offHand.render },
                "left-arm":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.armor.render+"/left-arm" },
                "left-hand":{ "attachment": aspect.race+"/"+aspect.sex+"/"+equip.gloves.render+"/left-hand" },
                "front-off-hand":{ "attachment": "weapons/"+equip.shield.render }
            };
            var partIndex = 0;
            for (var part in config){
                var attachmentName = config[part].attachment;
                if(!avatar.skeleton.findSlot(part))  continue;
                if(attachmentName){
                    avatar.skeleton.findSlot(part).attachment = avatar.skeleton.data.skins[0].attachments[partIndex+":"+attachmentName];
                }
                partIndex++;
                if(config[part].color) {
                    var rgbColor = config[part].color;
                    avatar.skeleton.findSlot(part).r = rgbColor.red/255;
                    avatar.skeleton.findSlot(part).g = rgbColor.green/255;
                    avatar.skeleton.findSlot(part).b = rgbColor.blue/255;
                }
            }
        },
        update : function (GAME) {
            GAME.socket.update(this.GAME);
            this.loadAvatarConfig(this.avatar,this.defaultConfig);
        },
        saveSlot : function (slot,index){
            this.GAME.socket.emit("createSlot",{ data:slot,index:index});
            window.isOldSlots = true;
            this.game.state.start('selectCharacter');
        }
    };
    window['phaser'] = window['phaser'] || {};
    window['phaser'].createCharacter = createCharacter;

}());