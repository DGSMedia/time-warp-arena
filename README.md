Time warp Arena

Game Type  : Fighting action platform online
Game mode : (demo), 1v1, 2vs2
Story : “” 

Gameplay :
Every player once the game start have to create it’s own avatar, this avatar can have different race, class, equipment,weapon and skill, they can be bought  through the shop with in game currency, or just level up the avatar.
Players can be also customize the outfit of their avatar with fashion items.
Every match will give as rewards, money or mats for build and craft powerful equipment or learn new skills.
The scope of the game it’s kill your opponents more time you can in [DEFINE TIME] mins using your ability or environment traps
Every player will have their skill set  decided by them before the match begin so it will be no possible to change weapon during a match.
Sometimes will be necessary to coop with your team-mate to unlock secret areas or boosts hidden on the maps.
Every equipment have durability and it will need repairs before get destroyed and lost for ever.
It will be possible play with Friends if they are online against or cooperative.
It will be possibile to trade items (mats and equipment).

Avatar: 

Race : humans, orcs, undeads, aliens ( multiple type), robots, spirits.
Every race start with different stats point, and every stats points give different advantage in game.
Classes : 
Basic: Melee fighter, ranged fighter, mage.
Others: tank,samurai, buffer, gladiator, summoner, rogue, etc …
Every class have it’s own weapons, skills,animations and equipments.
The equipments is made by:
helmet
chest
gloves
boots
weapon
off-hand
customizations : skin color, hair color, sex (where it’s available) 

Level up:  
Every level up you gain:
 1 point for increase the stats  ( define stats)
 random equip box