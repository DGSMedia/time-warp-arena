function Room(roomId) {
    this.id = roomId.toString();
    this.players = [];
    this.status = 1;            // 1 open, 2 inGame, 3 end
    this.score = {
        red: 0,
        blue:0
    };
}

module.exports = Room;