var Clock = function(){
    var clock = this;
    this.seconds = 60;
    this.functions = {tick :[],end: []};
    this.reset = reset;
    this.play = play;
    this.stop = stop;
    this.time = formatTime();
    this.setTime = setTime;
    this.onTick = onTick;
    this.onEnd = onEnd;

    function tick(){
        clock.seconds--;
        clock.time = formatTime();
        clock.functions.tick.forEach(function(fun){fun(clock);});
        if (clock.seconds <= 0 )  clock.stop();
    }
    function play(seconds){
        if(seconds) setTime(seconds);
        clock.interval= setInterval(tick,1000);
    }
    function stop(){
        clock.functions.end.forEach(function(fun){fun(clock);});
        clock.seconds = 0;
        clearInterval(clock.interval);
    }
    function reset () {
        clock.functions = { tick :[], end: [] };
        clock.stop();
    }
    function formatTime(){
        var date = new Date(clock.seconds * 1000);
        var mm = date.getUTCMinutes();
        var ss = date.getSeconds();
        if (mm < 10) {mm = "0"+mm;}
        if (ss < 10) {ss = "0"+ss;}
        return mm+":"+ss; // This formats your string to MM:SS
    }
    function setTime(seconds){
        clock.seconds = seconds;
    }
    function onTick(fun){
        clock.functions.tick.push(fun);
    }
    function onEnd(fun){
        clock.functions.end.push(fun);
    }
    return this;
};
module.exports = Clock;
/*
var test = new Clock();
test.onTick(function(clock){console.log("tick"+clock.seconds);});
test.onEnd(function(clock){console.log("done"+clock.seconds);});
test.play(3);
*/