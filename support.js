/**
 * Created by stefano on 12/10/16.
 */
var http = new XMLHttpRequest();
var url = "http://localhost:3000/register";
var params = {
    "email":"test@test.com",
    "psw":"psw",
    "username":"test"
};
http.open("POST", url, true);
http.setRequestHeader("Content-type", "application/json");
http.onload = function(){
    console.log("done");
};
http.send(JSON.stringify(params));


var http = new XMLHttpRequest();
var url = "http://localhost:3000/login";
var params = {
    "psw":"psw",
    "username":"test"
};
http.open("POST", url, true);
http.setRequestHeader("Content-type", "application/json");
http.onload = function(){
    console.log("done");
};
http.send(JSON.stringify(params));


var Clock = function(){
 var clock = this;
 this.count = 0;
 this.play = play;
 this.stop = stop;
 this.reset = reset;
 this.time = formatTime();
 function increase(){
 clock.count++;
 clock.time = formatTime();
 }
 function play(){
 clock.interval= setInterval(increase,1000);
 }
 function stop(){
 clearInterval(clock.interval);
 }
 function reset(){
 clock.count = 0;
 clock.time = formatTime();
 }
 function formatTime(){
 var date = new Date(clock.count * 1000);
 var mm = date.getUTCMinutes();
 var ss = date.getSeconds();
 if (mm < 10) {mm = "0"+mm;}
 if (ss < 10) {ss = "0"+ss;}
 return mm+":"+ss; // This formats your string to MM:SS
 }
 };



broadCastEvent : function (dataStr){
    var GAME = this.GAME;
    var msg = Tools.getJson(dataStr);
    var playerId = (msg.data && msg.data.player && msg.data.player.id)? msg.data.player.id : msg.sender;
    var player = GAME.players && GAME.players[playerId];
    if (!player || !msg.event) return null;
    if (msg.event === "playerMove") player.updatePosition(msg.data);
    if (msg.event && msg.event === "playerStatus")  player.updateStatus(msg.data.status,msg.duration);
    if (msg.event && msg.event === "playerSkill")   player.skills[msg.data.skillIndex].execute(player);
    if (msg.event && msg.event === "syncEvent") {
        if( msg.data.player.x > player.x +15 || msg.data.player.x < player.x-15){
            player.x = msg.data.player.x;
            player.y = msg.data.player.y;
        }
        player.health = msg.data.player.health;
        player.alive = msg.data.player.alive;
    }
}
