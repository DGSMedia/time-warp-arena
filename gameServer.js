var io = require("socket.io");
var Room = require('./src/room.js');
var socket = io.listen(3000);
function startServer (){

    var people = {};
    var clients = {};
    var rooms = {};
    var waitingList = [];

    socket.on("connection", function (client) {
        client.leaveQueue = leaveQueue;
        client.joinQueue = joinQueue;
        client.on("registerClient", registerClient);
        client.on("disconnect", disconnect);
        client.on("joinQueue", joinQueue);
        client.on("leaveQueue",leaveQueue);
        client.on("leaveMatch",leaveMatch);
        client.on("closeRoom",closeRoom);
        client.on('readyToPlay',readyToPlay);
        client.on('updateScore',updateScore);
        client.on('broadCastEvent',broadCastEvent);
        client.on('setCharacterConfig',setCharacterConfig);
        client.on('getGameResult',getGameResult);

        function registerClient (username) {
            if (!people[username]) people[username] = {"id": client.id, "room": "general", "status":1};
            else {
                people[username].id = client.id;
                people[username].status = 1;
            }
            client.username = username;
            clients[client.id] = client;
            socket.sockets.emit("logUpdate", username + " is now ONLINE");
            socket.sockets.emit("updatePeople", people);
        }
        function disconnect () {
            if (!people[client.username]) return;
            people[client.username].status = 0;
            leaveQueue ();
            if(rooms[people[client.username].room]) socket.sockets.in(people[client.username].room).emit("gameOver","win");
            socket.sockets.emit("update-people", people);
            socket.sockets.emit("logUpdate", client.username + " is now OFFLINE");
            delete clients[client.id];
        }
        function joinQueue () {
            client.join("waitingList");
            if(waitingList.indexOf(client.username) < 0) waitingList.push(client.username);
        }
        function leaveQueue () {
            client.leave("waitingList");
            var index = waitingList.indexOf(client.username);
            if (index >= 0) {
                waitingList.splice(index,1);
                delete client.rooms["waitingList"];
            }
        }
        function leaveMatch (roomId) {
            roomId = (roomId)?roomId : people[client.username].room;
            if(!rooms[roomId]) return; // investigate this issue deeper
            Object.keys(rooms[roomId].players).forEach(function (username) {
                var user = people[username];
                user.room = "general";
                user.status = 1;
            });
            rooms[roomId].status = 3;
            socket.sockets.in(roomId).emit("roomExpire");
            socket.sockets.emit("update-people", people);
        }
        function closeRoom () {
            var user = people[client.username];
            if(!rooms[user.room]) return;
            rooms[user.room].status = 3;
            client.emit("roomExpire");
            user.room = "general";
            user.status = 1;
            socket.sockets.emit("update-people", people);
        }
        function readyToPlay () {
            var peopleReady = 0;
            var user = people[client.username];
            var room = rooms[user.room];
            var playersId = Object.keys(rooms[user.room].players);
            var player = rooms[user.room].players[client.username];
            user.status = 2;
            room.host = (!room.host) ? client.username : room.host;
            socket.sockets.in(user.room).emit("logUpdate", client.username + " is ready to play!");
            socket.sockets.in(user.room).emit("setRoomHost",room.host);
            for (var p = 0; p < playersId.length; p++) {
                if (people[playersId[p]].status === 2) peopleReady++;
            }
            if( !player.team) player.team = (peopleReady % 2)? "red" : "blue";
            if (peopleReady === 2) {
                socket.sockets.in(user.room).emit("logUpdate", "All are ready to play!");
                socket.sockets.in(user.room).emit("startGame",JSON.stringify(room));
                room.status = 2;
            }
            socket.sockets.emit("update-people", people);
        }
        function updateScore (scoreStr) {
            var room = rooms[people[client.username].room];
            room.score = JSON.parse(scoreStr);
            socket.sockets.in(room.id).emit("updateScore",scoreStr);
        }
        function broadCastEvent (dataStr) {
            var room = people[client.username] && people[client.username].room;
            if (!room.events) room.events = [];
            room.events.push(dataStr);
            var msg = JSON.parse(dataStr);
            if (room && msg) socket.sockets.in(room).emit("broadCastEvent",dataStr);
        }
        function setCharacterConfig(characterObject){
            if(!people[client.username]) return;
            people[client.username].character = JSON.parse(characterObject);
            // get inventory from database
            // check valid config
            // save config
            socket.sockets.emit("updatePeople", people);
        }
        function getGameResult(){
            var roomId = people[client.username].room;
            var room = rooms[roomId];
            var clientTeam = room.players[client.username].team;
            var result;
            var teamsSorted = Object.keys(room.score).sort(function(a,b){return room.score[b]-room.score[a]});
            if (teamsSorted[0] === clientTeam && room.score[clientTeam] > room.score[teamsSorted[1]]) result = "win";
            else if (room.score[clientTeam]  ===  room.score[teamsSorted[0]]) result = "draw";
            else result = "lose";
            client.emit("updateFinalScore",JSON.stringify({score:room.score,result:result}));
        }
    });
    setInterval(matchPlayers,1500);
    function matchPlayers (){
        var playersIds = [];
        var playerId1 = waitingList[Math.floor(Math.random()*waitingList.length)];
        var waitingListTemp = waitingList.filter(function(obj) { return [playerId1].indexOf(obj) === -1; });
        var playerId2 = waitingListTemp[Math.floor(Math.random()*waitingListTemp.length)];
        if (!playerId1 || !playerId2) return null;
        playersIds.push(playerId1);
        playersIds.push(playerId2);
        var room = createRoom(playersIds);
        socket.sockets.emit("updatePeople", people);
        socket.sockets.in(room.id).emit("setRoom", rooms[room.id]);
    }
    function createRoom (playersIds) {
        var room = new Room("R"+new Date()*10000+parseInt(Math.random()*100));
        room.players = {};
        rooms[room.id] = room;
        socket.sockets.emit("logUpdate", "new room created: "+room.id);
        playersIds.forEach(function(username){
            room.players[username] = {};
            var client = getClient(username);
            client.leaveQueue();
            client.join(room.id);
            people[username].room = room.id;
            socket.sockets.in(room.id).emit("logUpdate",client.username+" enter on the room "+ room.id);
        });
        return room;
    }
    function getClient (clientName) {
        if (!people[clientName]) return null;
        return  clients[people[clientName].id]
    }
    console.log(new Date(),"Server up and running!");
}
startServer();



